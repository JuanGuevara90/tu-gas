//
//  FuelStation.swift
//  TU GAS
//
//  Created by JUAN PABLO GUEVARA //  FABIAN CONSTANTE on 18/11/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import Foundation

class FuelStation: NSObject, NSCoding {
    var title: String!
    var address: String!
    var latitude: Double!
    var longitude: Double!
    var brandId: Int!
    var districtId: Int!
    var municipalityId: Int!
    var prices: [Int:Double]!
    var distance:Double!
    var active = true 
    
    init(title: String, address: String, latitude: Double, longitude: Double, brandId: Int, districtId: Int, municipalityId: Int, prices: [Int:Double],distance:Double){
        self.title = title
        self.address = address
        self.latitude = latitude
        self.longitude = longitude
        self.brandId = brandId
        self.districtId = districtId
        self.municipalityId = municipalityId
        self.prices = prices
        self.distance=distance
    }
    
    struct Const {
        static let title = "title"
        static let address = "address"
        static let latitude = "latitude"
        static let longitude = "longitude"
        static let brandId = "brandId"
        static let districtId = "districtId"
        static let municipalityId = "municipalityId"
        static let prices = "prices"
        static let distance = "distance"
    }
    
    // MARK: NSCoding
    func encode(with coder: NSCoder) {
        coder.encode(title, forKey: Const.title)
        coder.encode(address, forKey: Const.address)
        coder.encode(latitude, forKey: Const.latitude)
        coder.encode(longitude, forKey: Const.longitude)
        coder.encode(brandId, forKey: Const.brandId)
        coder.encode(districtId, forKey: Const.districtId)
        coder.encode(municipalityId, forKey: Const.municipalityId)
        coder.encode(prices, forKey: Const.prices)
        coder.encode(distance, forKey: Const.distance)
    }
    
    required init?(coder decoder: NSCoder) {
        title = decoder.decodeObject(forKey: Const.title) as! String
        address = decoder.decodeObject(forKey: Const.address) as! String
        latitude = decoder.decodeObject(forKey: Const.latitude) as! Double
        longitude = decoder.decodeObject(forKey: Const.longitude) as! Double
        brandId = decoder.decodeObject(forKey: Const.brandId) as! Int
        districtId = decoder.decodeObject(forKey: Const.districtId)  as! Int
        municipalityId = decoder.decodeObject(forKey: Const.municipalityId)  as! Int
        prices = decoder.decodeObject(forKey: Const.prices) as! [Int:Double]
        let nat = decoder.decodeObject(forKey: Const.distance) as? Double
        if let n = nat {
            distance = n
        } else {
            distance = 0
        }
    }
    
    static func saveMany (_ brands: [FuelStation]) -> Bool {
        let documentsPath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        
        let filePath = documentsPath.appendingPathComponent("FuelStations.data")
        print(filePath)
        let path = filePath.path
        
        if NSKeyedArchiver.archiveRootObject(brands, toFile: path) {
            return true
        }
        return false
    }
    
    static func loadAll()  -> [FuelStation] {
        var dataToRetrieve = [FuelStation]()
        
        let documentsPath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        let filePath = documentsPath.appendingPathComponent("FuelStations.data", isDirectory: false)
        
        let path = filePath.path
        
        if let newData = NSKeyedUnarchiver.unarchiveObject(withFile: path) as? [FuelStation] {
            dataToRetrieve = newData
        }
        return dataToRetrieve
    }
    
    
    
    
}
