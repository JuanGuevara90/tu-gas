//
//  ConsumeTableViController.swift
//  PROYECTO
//
//  Created by JUAN PABLO GUEVARA on 30/12/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import UIKit

class ConsumeTableViController: UITableViewController,UINavigationControllerDelegate {


    var fuelStation: FuelStation!
    var brand: Brand!
    var district: District!
    var municipality: Municipality!
    var fuelType: FuelType!
    var precio:Double!
    let date = Date()

    var mileaString = ""
    var totalPay = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UserDefaults.standard.set("" ,forKey: "mileage")
        UserDefaults.standard.set("",forKey: "totalPrice")
        UserDefaults.standard.set(self.precio ,forKey: "precioFuel")
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func viewDidAppear(_ animated: Bool) {
        
    }

    @IBAction func cancelAction(_ sender: Any) {
        UserDefaults.standard.set("" ,forKey: "mileage")
        UserDefaults.standard.set("",forKey: "totalPrice")
        UserDefaults.standard.set("",forKey: "precioFuel")
        dismiss(animated: true, completion: nil)
        
    }
  
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
       
        switch (section) {
        case 0:
            return "Brand"
        case 1:
            return "Date"
        case 2:
            let value = String(format:"%.0f", (ConsumeRepository.repository.consumes[ConsumeRepository.repository.consumes.count - 1].mileage)!)
            return "Last Mileage " + value + " km"
        default :
            return "Total to pay"
        }
    }
    
    
    @IBAction func saveAction(_ sender: Any) {
         //textFied is your textfield name.
       
       mileaString = UserDefaults.standard.string(forKey: "mileage")!
       totalPay = UserDefaults.standard.string(forKey: "totalPrice")!

        if(mileaString == "" && totalPay == ""){  // Campos con valor vacio
            print("Nilllll")
        }else{   // Campos con diferentes de vacio
            
            if(Double(mileaString) == nil  || Double(totalPay) == nil){  // Campos direntes de Double
                print("no es double")
            }else{          // Campos con tipo de Dato Double
                
                let newConsume = Consume(fuelStation: fuelStation, precioFuel: precio, date: date, mileage: Double(mileaString)!, totalPay: Double(totalPay)!, brand: brand,fuelType:fuelType)
                ConsumeRepository.repository.consumes.append(newConsume)
                ConsumeRepository.repository.save()
                UserDefaults.standard.set("" ,forKey: "mileage")
                UserDefaults.standard.set("",forKey: "totalPrice")
                self.dismiss(animated: true, completion: nil)
            }
            
        }
        
        // Crear Consume
    }
    
    func validateMileage() -> Double{
        var validate:Double = 0
        
        if (ConsumeRepository.repository.consumes.count == 0){ // Por primera vez valor de distancia
            return validate
        }else{ // Para una proxima vez
            let tam = ConsumeRepository.repository.consumes.count
            return ConsumeRepository.repository.consumes[tam-1].mileage!
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 4
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch (indexPath.section, indexPath.row) {
        case (0, 0):
            let cell = tableView.dequeueReusableCell(withIdentifier: "imagenCons") as! ImagenConsumeTableViewCell
            return cell.frame.height
        case (1, _):
            let cell = tableView.dequeueReusableCell(withIdentifier: "dateCons") as! DateConsumeTableViewCell
            return cell.frame.height
        case (2, 0) :
            let cell = tableView.dequeueReusableCell(withIdentifier: "mileageCons") as! MileageConsumeTableViewCell
            return cell.frame.height
            
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "priceCons") as! PriceConsumeTableViewCell
            return cell.frame.height
        }
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch (indexPath.section, indexPath.row) {
        case (0, 0):
            let cell = tableView.dequeueReusableCell(withIdentifier: "imagenCons", for: indexPath) as! ImagenConsumeTableViewCell
            return cell
        case (1, _):
            let cell = tableView.dequeueReusableCell(withIdentifier: "dateCons", for: indexPath) as! DateConsumeTableViewCell
            return cell
        case (2, 0) :
            let cell = tableView.dequeueReusableCell(withIdentifier: "mileageCons", for: indexPath) as! MileageConsumeTableViewCell
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "priceCons", for: indexPath) as! PriceConsumeTableViewCell
            return cell
        }
    }


    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        if let result = cell as? ImagenConsumeTableViewCell {
            result.imagenConsume.image = self.brand.image
            
        } else if let result = cell as? DateConsumeTableViewCell {
            let formatter = DateFormatter()
            //formatter.dateFormat = "dd/MM/yyyy"
            formatter.dateFormat = "EEEE, MMMM dd, yyyy' at 'h:mm a."
            let val = formatter.string(from: date)
            result.dateCons.text = val
        }
        
    }
    
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
