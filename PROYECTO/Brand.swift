//
//  Brand.swift
//  TU GAS
//
//  Created by JUAN PABLO GUEVARA  // FABIAN CONSTANTE on 18/11/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import Foundation
import UIKit


class Brand: NSObject, NSCoding {
    var id: Int!
    var name: String!
    var image: UIImage!
    var selected: Bool!
    
    init(id: Int, name: String, image: UIImage){
        self.id = id
        self.name = name
        self.image = image
        self.selected = true
    }
    
    struct Const {
        static let id = "id"
        static let name = "name"
        static let image = "image"
        static let selected = "selected"
    }
    
    // MARK: NSCoding
    func encode(with coder: NSCoder) {
        coder.encode(id, forKey: Const.id)
        coder.encode(name, forKey: Const.name)
        coder.encode(image, forKey: Const.image)
        coder.encode(selected, forKey: Const.selected)
    }
    
    required init?(coder decoder: NSCoder) {
        id = decoder.decodeObject(forKey: Const.id) as! Int
        name = decoder.decodeObject(forKey: Const.name) as! String
        image = decoder.decodeObject(forKey: Const.image) as! UIImage
        selected = decoder.decodeObject(forKey: Const.selected) as! Bool
    }
    
    static func saveMany (_ brands: [Brand]) -> Bool {
        let documentsPath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        
        let filePath = documentsPath.appendingPathComponent("Brands.data")
        print(filePath)
        let path = filePath.path
        
        if NSKeyedArchiver.archiveRootObject(brands, toFile: path) {
            return true
        }
        
        return false
    }
    
    static func loadAll()  -> [Brand] {
        var dataToRetrieve = [Brand]()
        
        let documentsPath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        let filePath = documentsPath.appendingPathComponent("Brands.data", isDirectory: false)
        
        let path = filePath.path
        
        if let newData = NSKeyedUnarchiver.unarchiveObject(withFile: path) as? [Brand] {
            dataToRetrieve = newData
        }
        return dataToRetrieve
}

}

 
