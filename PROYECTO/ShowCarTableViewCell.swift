//
//  ShowCarTableViewCell.swift
//  TU GAS
//
//  Created by JUAN PABLO GUEVARA // FABIAN CONSTANTE on 12/12/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import UIKit

class ShowCarTableViewCell: UITableViewCell {

    @IBOutlet weak var consumeLabel: UILabel!
    
    @IBOutlet weak var imagenCar: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
