//
//  StationTableViewCell.swift
//  TU GAS
//
//  Created by JUAN PABLO GUEVARA // FABIAN CONSTANTE  on 25/11/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import UIKit

class StationTableViewCell: UITableViewCell {

    @IBOutlet weak var titulo: UILabel!
    @IBOutlet weak var direccion: UILabel!
    @IBOutlet weak var brandImage: UIImageView!
    
    @IBOutlet weak var distance: UILabel!
    
    func setFuelStation (_ fuelStation: FuelStation, brands: [Brand]) {
        self.titulo.text = fuelStation.title
       var value:String=""

            value = String(format:"%.1f", fuelStation.distance)
            self.distance.text = "\(value) km"
        
            let valor = UserDefaults.standard.integer(forKey: "key")
            for fuel in fuelStation.prices{
                if(fuel.key == valor){
                    value="\(fuel.value) €"
                    self.direccion.text = value  // Valor de Gasolina
                }
            }
        
        for brand in brands {
            if(brand.id == fuelStation.brandId) {
                self.brandImage.image = brand.image
            }
        }
    }
    
    func setDistances (_ distance: String){
        self.distance.text = distance + "km"
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
