//
//  Data.swift
//  TU GAS
//
//  Created by JUAN PABLO GUEVARA  //  FABIAN CONSTANTE  on 28/12/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import Foundation

class LoadData{
    static let data = LoadData() //shared Instance
    fileprivate init () {}
    
    func cargar1(c:[Consume])->[(Double,Double)] {
        let date = Date()  //Fecha de Hoy
        let consumes = c // Todos los consumos
        var nowmonth:[(Consume,Int)]!

        var datos:[(Double,Double)] = [(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0)] // (Costo ,Distancia)
        
        nowmonth = [(Consume,Int)]()  // Encerar el arreglo nowmonth
        
        let calendar = Calendar.current
        let now = calendar.component(.month, from: date)
        
        for cos in consumes {
            let calendarTemporal = Calendar.current
            let temporal = calendarTemporal.component(.month, from: cos.date!)
            
            let calendarDay = Calendar.current
            let temmporalDay = calendarDay.component(.day, from: cos.date!)
            
            if((now - temporal) == 0){
                nowmonth.append(cos,temmporalDay) // Añadir cosumos del mes actual y de los dias
            }
        }
        
        if(nowmonth.count > 0){
            // Costos total por dia
            for now in nowmonth {
                datos[now.1 - 1].0 = datos[now.1 - 1].0 + now.0.totalPay!
            }
            
            // Distancia
            for i in  0 ..< nowmonth.count - 1
            {
                let j = nowmonth[i].1
                datos[j].1=datos[j].1 + nowmonth[i+1].0.mileage! - nowmonth[i].0.mileage!
            }
        }else{
            // No hay elementos en el arreglo
        }
        
        return datos
    }
    
    func cargar2(c:[Consume])->[(Int,Double,Double,Double,Double,Double)] {
        
        let consumes = c // Consumos totales del aplicativo
        let date = Date()  //Fecha de Hoy
        var datos:[(Int,Double,Double,Double,Double,Double)] = [(Int,Double,Double,Double,Double,Double)]()  //(Mes,PrimeraSemana,SegundaSemana,TerceraSemana,CuartaSemana,QuintaSemana)
        var nowmonth:[(Consume,Int)]!
        var previousmonth:[(Consume,Int)]!
        var months:[Int]!
        
        nowmonth = [(Consume,Int)]()
        previousmonth = [(Consume,Int)]()
        
        let calendar = Calendar.current
        let now = calendar.component(.month, from: date)
        months = valueMonth(nowMonth: now)
        
        // (For) para sacar la lista de consumo del mes anterior y actual en cada semana del mes correspondiente .....
        for cos in consumes {
            let calendarTemporal = Calendar.current
            let temporal = calendarTemporal.component(.month, from: cos.date!)
            
            let calendarWeek = Calendar.current
            let temmporalWeek = calendarWeek.component(.weekOfMonth, from: cos.date!)
            
            switch now {
            case 1: // Mes actual Enero
                switch now - temporal {
                case 0: // Enero temporal
                    nowmonth.append(cos,temmporalWeek)
                default :  // Diciembre temporal
                    if (temporal == 12){
                        previousmonth.append(cos,temmporalWeek)
                    }
                }
                
            case 12:  //Mes actual Diciembre
                switch temporal {
                case 12:
                    nowmonth.append(cos,temmporalWeek)  // Mes actual
                default:
                    if(now-temporal == 1){  // Anterior mes
                        previousmonth.append(cos,temmporalWeek)
                    }
                }
                
            default:  // Mes cualquiera
                switch temporal-now {
                case 0:
                    nowmonth.append(cos,temmporalWeek) // Mes actual
                default:
                    if(temporal-now == -1){
                        previousmonth.append(cos,temmporalWeek)  // Mes anterior
                    }
                }
            }
            
        }
        
        // Calculo de distacia por lo menos 2 cosumos en el mes
        // Mes anterior
        var pricetotalFirstWeekBefo:Double = 0
        var pricetotalSecondtWeekBefo:Double = 0
        var pricetotalThirdWeekBefo:Double = 0
        var pricetotalFourthWeekBefo:Double = 0
        var pricetotalFivethtWeekBefo:Double = 0
        if (previousmonth.count >= 1){
            
            for j in  0 ..< previousmonth.count
            {
                switch previousmonth[j].1 {
                case 1:
                    pricetotalFirstWeekBefo = pricetotalFirstWeekBefo + previousmonth[j].0.totalPay!
                case 2:
                    pricetotalSecondtWeekBefo = pricetotalSecondtWeekBefo + previousmonth[j].0.totalPay!
                case 3:
                    pricetotalThirdWeekBefo = pricetotalThirdWeekBefo + previousmonth[j].0.totalPay!
                case 4:
                    pricetotalFourthWeekBefo = pricetotalFourthWeekBefo + previousmonth[j].0.totalPay!
                case 5:
                    pricetotalFivethtWeekBefo = pricetotalFivethtWeekBefo + previousmonth[j].0.totalPay!
                default:
                    break;
                }
                
            }
            
        }else{
            pricetotalFirstWeekBefo = 0
            pricetotalSecondtWeekBefo = 0
            pricetotalThirdWeekBefo = 0
            pricetotalFourthWeekBefo = 0
            pricetotalFivethtWeekBefo = 0
        }
        
        // Calculo de valor pagado mes anterior
        
        datos.append((months[0],pricetotalFirstWeekBefo,pricetotalSecondtWeekBefo,pricetotalThirdWeekBefo,pricetotalFourthWeekBefo,pricetotalFivethtWeekBefo))
        
        //--------------------------------------------------------------------------------------------
        
        // Calculo de distacia
        // Mes actual
        
        var pricetotalFirstWeekNow:Double = 0
        var pricetotalSecondWeekNow:Double = 0
        var pricetotalThirdWeekNow:Double = 0
        var pricetotalFourthWeekNow:Double = 0
        var pricetotalFivethWeekNow:Double = 0
        
        
        if (nowmonth.count >= 1){
            
            for j in  0 ..< nowmonth.count
            {
                switch nowmonth[j].1 {
                case 1:
                    pricetotalFirstWeekNow = pricetotalFirstWeekNow + nowmonth[j].0.totalPay!
                case 2:
                    pricetotalSecondWeekNow = pricetotalSecondWeekNow + nowmonth[j].0.totalPay!
                case 3:
                    pricetotalThirdWeekNow = pricetotalThirdWeekNow + nowmonth[j].0.totalPay!
                case 4:
                    pricetotalFourthWeekNow = pricetotalFourthWeekNow + nowmonth[j].0.totalPay!
                case 5:
                    pricetotalFivethWeekNow = pricetotalFivethWeekNow + nowmonth[j].0.totalPay!
                default:
                    break;
                    
                }
            }
        }else{
            pricetotalFirstWeekNow = 0
            pricetotalSecondWeekNow = 0
            pricetotalThirdWeekNow = 0
            pricetotalFourthWeekNow = 0
            pricetotalFivethWeekNow = 0
        }
        
        
        // Calculo de valor pagado mes anterior
        
        datos.append((now,pricetotalFirstWeekNow,pricetotalSecondWeekNow,pricetotalThirdWeekNow,pricetotalFourthWeekNow,pricetotalFivethWeekNow))
        
        //-----------------------------------------------------------------------------------------------
        // Valores para el proximo mes  promedio de cada semana
        
        var priceAverageFirstWeek:Double = 0
        var priceAverageSecondtWeek:Double = 0
        var priceAverageThirdWeek:Double = 0
        var priceAverageFourthWeek:Double = 0
        var priceAverageFivethWeek:Double = 0
        
        if (pricetotalFirstWeekBefo > 0 && pricetotalFirstWeekNow > 0){   // Precio de primera semana
            priceAverageFirstWeek = (pricetotalFirstWeekBefo+pricetotalFirstWeekNow) / 2
        }else{
            // First Week
            if(pricetotalFirstWeekBefo == 0 || pricetotalFirstWeekNow == 0){
                if(pricetotalFirstWeekBefo == 0 && pricetotalFirstWeekNow > 0 ){
                    priceAverageFirstWeek = pricetotalFirstWeekNow  // Precio promedio es igual a la actual
                }else{
                    priceAverageFirstWeek = pricetotalFirstWeekBefo  // Precio promedio es igual al anterio
                }
            }
        }
        
        
        if (pricetotalSecondtWeekBefo > 0 && pricetotalSecondWeekNow > 0){   // Precio de segunda semana
            priceAverageSecondtWeek = (pricetotalSecondtWeekBefo+pricetotalSecondWeekNow) / 2

        }else{
            // First Week
            if(pricetotalSecondtWeekBefo == 0 || pricetotalSecondWeekNow == 0){
                if(pricetotalSecondtWeekBefo == 0 && pricetotalSecondWeekNow > 0 ){
                    priceAverageSecondtWeek = pricetotalSecondWeekNow  // Precio promedio es igual a la actual
                }else{
                    priceAverageSecondtWeek = pricetotalSecondtWeekBefo  // Precio promedio es igual al anterio
                }
            }
        }
       
        
        if (pricetotalThirdWeekBefo > 0 && pricetotalThirdWeekNow > 0){   // Precio de tercera semana
            priceAverageThirdWeek = (pricetotalThirdWeekBefo+pricetotalThirdWeekNow) / 2
        }else{
            // First Week
            if(pricetotalThirdWeekBefo == 0 || pricetotalThirdWeekNow == 0){
                if(pricetotalThirdWeekBefo == 0 && pricetotalThirdWeekNow > 0 ){
                    priceAverageThirdWeek = pricetotalThirdWeekNow  // Precio promedio es igual a la actual
                }else{
                    priceAverageThirdWeek = pricetotalThirdWeekBefo  // Precio promedio es igual al anterio
                }
            }
        }
        
        if (pricetotalFourthWeekBefo > 0 && pricetotalFourthWeekNow > 0){   // Precio de cuarta semana
            priceAverageFivethWeek = (pricetotalFivethtWeekBefo+pricetotalFivethWeekNow) / 2
        }else{
            // First Week
            if(pricetotalFourthWeekBefo == 0 || pricetotalFourthWeekNow == 0){
                if(pricetotalFourthWeekBefo == 0 && pricetotalFourthWeekNow > 0 ){
                    priceAverageFourthWeek = pricetotalFourthWeekNow  // Precio promedio es igual a la actual
                }else{
                    priceAverageFourthWeek = pricetotalFourthWeekBefo  // Precio promedio es igual al anterio
                }
            }
        }

        
        if (pricetotalFivethtWeekBefo > 0 && pricetotalFivethWeekNow > 0){   // Precio de quinta semana
            priceAverageFivethWeek = (pricetotalFivethtWeekBefo+pricetotalFivethWeekNow) / 2
        }else{
            // First Week
            if(pricetotalFivethtWeekBefo == 0 || pricetotalFivethWeekNow == 0){
                if(pricetotalFivethtWeekBefo == 0 && pricetotalFivethWeekNow > 0 ){
                    priceAverageFivethWeek = pricetotalFivethWeekNow  // Precio promedio es igual a la actual
                }else{
                    priceAverageFivethWeek = pricetotalFivethtWeekBefo  // Precio promedio es igual al anterio
                }
            }
        }
        
        
        datos.append((months[1],priceAverageFirstWeek,priceAverageSecondtWeek,priceAverageThirdWeek,priceAverageFourthWeek,priceAverageFivethWeek))
        
        return datos
    }
    
    
    func cargar3(c:[Consume])->[(Int,Double,Double)] {
        let consumes = c
        let date = Date()  //Fecha de Hoy
        
        var datos:[(Int,Double,Double)] = [(Int,Double,Double)]()
        var nowmonth:[(Consume)]!
        var previousmonth:[(Consume)]!
        var months:[Int]!   // Meses anterior y siguiente
        
        nowmonth = [Consume]()
        previousmonth = [Consume]()
        
        let calendar = Calendar.current
        let now = calendar.component(.month, from: date)
        months=valueMonth(nowMonth: now)
        
        for cos in consumes {
            let calendarTemporal = Calendar.current
            let temporal = calendarTemporal.component(.month, from: cos.date!)
            
            switch now {
            case 1: // Mes actual Enero
                switch now - temporal {
                case 0: // Enero temporal
                    nowmonth.append(cos)
                default :  // Diciembre temporal
                    if (temporal == 12){
                        previousmonth.append(cos)
                    }
                }
                
            case 12:  //Mes actual Diciembre
                switch temporal {
                case 12:
                    nowmonth.append(cos)  // Mes actual
                default:
                    if(now-temporal == 1){  // Anterior mes
                        previousmonth.append(cos)
                    }
                }
                
            default:  // Mes cualquiera
                switch temporal-now {
                case 0:
                    nowmonth.append(cos) // Mes actual
                default:
                    if(temporal-now == -1){
                        previousmonth.append(cos)  // Mes anterior
                    }
                }
            }
            
        }
        
        // Calculo de distacia por lo menos 2 cosumos en el mes
        // Mes anterior
        var distanceTotalMonthBefo:Double = 0
        var pricetotalMonthBefo:Double = 0
        if (previousmonth.count >= 2){
            
            for i in  0 ..< previousmonth.count - 1
            {
                distanceTotalMonthBefo = distanceTotalMonthBefo + previousmonth[i+1].mileage! - previousmonth[i].mileage!
            }
            
            for j in  0 ..< previousmonth.count
            {
                pricetotalMonthBefo = pricetotalMonthBefo + previousmonth[j].totalPay!
            }
            
        }else{
            if(previousmonth.count == 1){
                distanceTotalMonthBefo = 0
                for j in  0 ..< previousmonth.count
                {
                    pricetotalMonthBefo = pricetotalMonthBefo + previousmonth[j].totalPay!
                }
            }else{
                distanceTotalMonthBefo = 0
                pricetotalMonthBefo = 0
            }
        }
        
        // Calculo de valor pagado mes anterior
        
        datos.append((months[0],distanceTotalMonthBefo,pricetotalMonthBefo))
        
        
        //--------------------------------------------------------------------------------------------
        
        // Calculo de distacia
        // Mes actual
        var distanceTotalMonthNow:Double = 0
        var pricetotalMonthNow:Double = 0
        if (nowmonth.count >= 2){
            
            for i in  0 ..< nowmonth.count - 1
            {
                distanceTotalMonthNow = distanceTotalMonthNow + nowmonth[i+1].mileage! - nowmonth[i].mileage!
            }
            for j in  0 ..< nowmonth.count
            {
                pricetotalMonthNow = pricetotalMonthNow + nowmonth[j].totalPay!
            }
            
        }else{
            
            if(nowmonth.count == 1){
                
                distanceTotalMonthNow = 0
                
                for j in  0 ..< nowmonth.count
                {
                    pricetotalMonthNow = pricetotalMonthNow + nowmonth[j].totalPay!
                }
            }else{
                distanceTotalMonthNow = 0
                pricetotalMonthNow = 0
            }
            
        }
        
        // Calculo de valor pagado mes anterior
        
        datos.append((now,distanceTotalMonthNow,pricetotalMonthNow))
        
        //-----------------------------------------------------------------------------------------------
        // Valores para el proximo mes  promedio
        
        var distanceAverage:Double = 0
        var priceAverage:Double = 0
        
        if(distanceTotalMonthBefo > 0 && distanceTotalMonthNow > 0){
            distanceAverage = (distanceTotalMonthBefo+distanceTotalMonthNow) / 2
        }else{
            if(distanceTotalMonthBefo == 0 || distanceTotalMonthNow == 0 ){
                if(distanceTotalMonthBefo == 0 && distanceTotalMonthNow > 0){
                    distanceAverage = distanceTotalMonthNow
                }else{
                    distanceAverage = distanceTotalMonthBefo
                }
            }
        }
        
        
        if (pricetotalMonthBefo > 0 && pricetotalMonthNow > 0){
            priceAverage = (pricetotalMonthBefo+pricetotalMonthNow) / 2
        }else{
            if(pricetotalMonthBefo == 0 || pricetotalMonthNow == 0){
                if(pricetotalMonthBefo == 0 && pricetotalMonthNow > 0) {
                    priceAverage = pricetotalMonthNow
                }else{
                    priceAverage = pricetotalMonthBefo
                }
            }
        }
        
        datos.append((months[1],distanceAverage,priceAverage))
        
        return datos
    }
    
    func valueMonth(nowMonth:Int) ->[Int]{
        let now = nowMonth
        var arreglo:[Int] = [0,1]
        switch nowMonth {
        case 1: // Mes actual
            let before = 12 // anterior
            let after = now+1  // proyeccion
            arreglo[0] = before  // Posicion 0 before
            arreglo[1] = after     // Posicion 1 after
            return arreglo
        case 12:  //Mes actual
            let before:Int = now-1 // anterior
            let after = 1 //  Proyeccion Enero
            arreglo[0] = before  // Posicion 0 before
            arreglo[1] = after     // Posicion 1 after
            return arreglo
            
        default:
            let before = now-1  // anterior para los otros casos
            let after = now+1  // proyeccion
            arreglo[0] = before  // Posicion 0 before
            arreglo[1] = after   // Posicion 1 after
            return arreglo
        }
    }
    
    
    func getMonth(month:Int) -> String {
        let mounths:[String]=["Jan","Feb","Mar","Abr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]
        return mounths[month-1]
    }
    
}

