//
//  ShowFuelStationTableViewController.swift
//  TU GAS
//
//  Created by JUAN PABLO GUEVARA   //  FABIAN CONSTANTE   on 1/12/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import Social

class ShowFuelStationTableViewController: UITableViewController, CLLocationManagerDelegate {

    var fuelStation: FuelStation!
    var brand: Brand!
    var district: District!
    var municipality: Municipality!
    var fuelTypes = [FuelType]()
    var distance = "..."
    let showConsume = "showConsume"
    var precio:Double!
    let locationManager = CLLocationManager()
    var currentLocation = CLLocationCoordinate2D()
    
    @IBAction func buttonOpcion(_ sender: Any) {
        
        let actionSheet = UIAlertController(title: "", message: "Share this fuel station", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let twitterAction = UIAlertAction(title: "Share on Twitter", style: .default){ (action) -> Void in
            if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeTwitter){
                if let twitterVC = SLComposeViewController(forServiceType: SLServiceTypeTwitter) {
                    //protect empty first name
                    twitterVC.setInitialText("I went to \(self.brand.name) called \(self.fuelStation.title) at \(self.municipality.name), \(self.district.name).")
                    self.present(twitterVC, animated: true, completion: nil)
                }
            } else {
                print("You must first set up a Twitter account.")
                self.sharingError(message: "You must first set up a Twitter account.")
            }
        }
        
        
        let facebookAction = UIAlertAction(title: "Share on Facebook", style: UIAlertActionStyle.default) { (action) -> Void in
            if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeFacebook) {
                let facebookVC = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
                facebookVC?.setInitialText("I went to \(self.brand.name) called \(self.fuelStation.title) at \(self.municipality.name), \(self.district.name).")
                self.present(facebookVC!, animated: true, completion: nil)
                
            }else{
                self.sharingError(message: "You must first login to your Facebook account.")
            }
        }

        
        let dismissAction = UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.cancel) { (action) -> Void in
            
        }
        
        actionSheet.addAction(twitterAction)
        actionSheet.addAction(facebookAction)
        actionSheet.addAction(dismissAction)
        
        present(actionSheet, animated: true, completion: nil)

    }
    
    
    @IBAction func bringAction(_ sender: Any) {
        let latitute:CLLocationDegrees =  self.fuelStation.latitude
        let longitute:CLLocationDegrees =  self.fuelStation.longitude
        
        let regionDistance:CLLocationDistance = 10000
        let coordinates = CLLocationCoordinate2DMake(latitute, longitute)
        
        let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = self.brand.name
        mapItem.openInMaps(launchOptions: options)
    }
    
    func sharingError (message:String) -> Void {
        let alert = UIAlertController(title: "Sharing error", message:
            message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style:
            UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated:  true, completion:  nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //locationInit()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 4
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1 {
            return fuelStation.prices.count
        } else if section == 2 {
            return 1
        } else {
            return 1
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch (section) {
        case 0:
            return "Main Information"
        case 1:
            return "Prices"
        case 2:
            return "Location"
        default :
            return ""
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch (indexPath.section, indexPath.row) {
        case (0, 0):
            let cell = tableView.dequeueReusableCell(withIdentifier: "fuelStationInfo") as! FuelStationInfoTableViewCell
            return cell.frame.height
        case (1, _):
            let cell = tableView.dequeueReusableCell(withIdentifier: "fuelStationPrice") as! FuelStationPriceTableViewCell
            return cell.frame.height
        case (2, 0) : 
            let cell = tableView.dequeueReusableCell(withIdentifier: "fuelStationLocation") as! FuelStationLocationTableViewCell
            return cell.frame.height
            
        default: 
            let cell = tableView.dequeueReusableCell(withIdentifier: "fuelStationMap") as! FuelStationMapTableViewCell
            return cell.frame.height
        }
    }
    

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch (indexPath.section, indexPath.row) {
        case (0, 0):
            let cell = tableView.dequeueReusableCell(withIdentifier: "fuelStationInfo", for: indexPath) as! FuelStationInfoTableViewCell
            return cell
        case (1, _):
            let cell = tableView.dequeueReusableCell(withIdentifier: "fuelStationPrice", for: indexPath) as! FuelStationPriceTableViewCell
            return cell
        case (2, 0) :
                let cell = tableView.dequeueReusableCell(withIdentifier: "fuelStationLocation", for: indexPath) as! FuelStationLocationTableViewCell
                return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "fuelStationMap", for: indexPath) as! FuelStationMapTableViewCell
            return cell
        }
    }
    
    
   
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let valor = UserDefaults.standard.integer(forKey: "key")
        
        
        if let result = cell as? FuelStationMapTableViewCell {
                result.latitude = fuelStation.latitude
                result.longitude = fuelStation.longitude
                result.stationName = fuelStation.title
                result.brandName = brand.name
                result.setMapView()  // cargar el mapa

        } else if let result = cell as? FuelStationInfoTableViewCell {
            result.addressLabel.text = fuelStation.address
            result.brandLabel.text = brand.name
            result.fuelStationImageView.image = brand.image
            result.nameLabel.text = fuelStation.title
        } else if let result = cell as? FuelStationPriceTableViewCell {
            var index = 0
            for (key, value) in fuelStation.prices {
                if index == indexPath.row {
                    result.priceLabel.text = "\(value) €"
                    for fuelType in fuelTypes {
                        if fuelType.id == key {
                            result.typeLabel.text = fuelType.name
                            if valor==fuelType.id {
                                
                         //       result.setSelected(true, animated: true)
                                result.accessoryType = UITableViewCellAccessoryType.checkmark
                            }
                            
                        }
                    }
                }
                index += 1
            }
       }else if let result = cell as? FuelStationLocationTableViewCell {
            var valueDistance:String=""
            valueDistance = String(format:"%.1f", fuelStation.distance)
            var municapalityName:String=""
            municapalityName = municipality.name
            var districtName:String=""
            districtName = district.name
            result.districtLabel.text = "\(municapalityName), \(districtName)"
            result.distanceLabel.text = "\(valueDistance)km"
        }
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Seccion 2 seleccionada
        if indexPath.section==1 {
            var index = 0
            for (key, value) in fuelStation.prices {
                if index == indexPath.row {
                    precio=value
                    for fuelType in fuelTypes {
                        if fuelType.id == key {
                            performSegue(withIdentifier: showConsume ,sender: fuelType)
                        }
                    }
                }
                index += 1
            }

        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if segue.identifier == showConsume {
            //if let consumeViewController = segue.destination as? ConsumeViewController {
            if let nav = segue.destination as? UINavigationController{
                if let consumeViewController = nav.topViewController as?ConsumeTableViController {
                    if let fuelType = sender as? FuelType {
                        consumeViewController.precio = precio
                        consumeViewController.fuelType = fuelType
                        consumeViewController.fuelStation = fuelStation
                        consumeViewController.brand = brand
                        consumeViewController.district = district
                        consumeViewController.municipality = municipality
                    }
                }
            }
            
            
        }

    }
    
    


}
