//
//  ArchivingRepository.swift
//  TU GAS
//
//  Created by JUAN PABLO GUEVARA // FABIAN CONSTANTE on 13/10/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import Foundation

class ArchivingRepository:RepositoryProtocol {
    
    //singleton repository
    static let repository = ArchivingRepository() //shared Instance
    fileprivate init () {}
    
    var userConf = [UserDefaul]()
    
    var documentsPath = URL(fileURLWithPath: "")
    var filePath = URL(fileURLWithPath: "")
    
    func save() {
        documentsPath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) [0])
        print(documentsPath)
        filePath = documentsPath.appendingPathComponent("userConf.data", isDirectory: false)
        print(filePath)
        
        let path = filePath.path
        
        if NSKeyedArchiver.archiveRootObject(userConf, toFile: path) {
            print("Successfully saved userconf")
        } else {
            print("Failure saving people")
        }
    }
    
    func load() {
        documentsPath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) [0])
        print(documentsPath)
        filePath = documentsPath.appendingPathComponent("userConf.data", isDirectory: false)
        print(filePath)
        
        let path = filePath.path
        
        if let newData = NSKeyedUnarchiver.unarchiveObject(withFile: path) as? [UserDefaul]{
            userConf = newData
        }
        
    }
}
