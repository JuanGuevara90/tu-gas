//
//  GeonameClient.swift
//  PROYECTO
//
//  Created by formando on 17/11/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import Foundation
import UIKit

class GeonameClient{
    /*
    static func fechCountry (countryCode:String, completion: @escaping (Country?) -> Void){
        //VALIDAR SI EXISTE LA URL
        if let url = URL(string: "http://194.210.216.190/stations") {
            //if let url = URL(string: "http://api.geonames.org/countryInfoJSON?formatted=true&lang=en&country=\(countryCode)&username=desenvolvimentoswift&style=full") {
            print("Inicia")
            let dataTask = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                print("Inicia 2")
                if data == nil {
                    completion(nil)
                    return
                }
                
                if let jsonDic = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String:Any]{
                    print("Inicia 3")
                    print(data)
                    
                    if let jsonArray = jsonDic["stations"] as? [[String:Any]] {
                        
                        print("JSON")
                        print(jsonArray)
                        for jsonCountry in jsonArray {
                            let country = Country.parse(json: jsonCountry)
                            completion(country)
                            return
                        }
                    }
                }
                completion(nil)
            })
            dataTask.resume()
        } else {
            completion(nil)
        }
    }
    */
    
    static func fechCountry2 (completion: @escaping ([FuelStation]?)-> Void) ->[FuelStation]!{
        
        var fuelStations = [FuelStation]()
        //VALIDAR SI EXISTE LA URL
        if let url = URL(string: "http://194.210.216.190/stations") {
            //if let url = URL(string: "http://api.geonames.org/countryInfoJSON?formatted=true&lang=en&country=\(countryCode)&username=desenvolvimentoswift&style=full") {
            print("Inicia")
            let dataTask = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                print("Inicia 2")
                if data == nil {
                    completion(nil)
                    return
                }
                
              
  //                  let resp = String(data:data!, encoding:String.Encoding.utf8)
    //                print(resp)
                    if let jsonDic = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String:Any] {
                    print("Inicia 3")
//                    print(data)
                    
                        for station in (jsonDic["stations"] as! [[String:Any]]) {
                        let prices = getPrices(station["prices"] as! [[String : Any]])
                        print("Inicia 4")
                            fuelStations.append(
                                FuelStation(
                                    title: station["title"] as! String,
                                    address: station["address"] as! String,
                                    latitude: station["latitude"] as! Double,
                                    longitude: station["longitude"] as! Double,
                                    brandId: station["brand_id"] as! Int,
                                    districtId: station["district_id"] as! Int,
                                    municipalityId: station["municipality_id"] as! Int,
                                    prices: prices!
                                )
                            )
                        }
                    
                    print(fuelStations[0].title)
                    
                       
                    
                    }
                    
               
                
                completion(nil)
            })
            dataTask.resume()
        } else {
            completion(nil)
            return nil
        }
        
        return fuelStations
    }
    
    
    static func getPrices(_ stationPrices: [[String:Any]]) -> [Int:Double]! {
        var prices = [Int:Double]()
        for price in stationPrices {
            prices[(price as AnyObject).object(forKey: "type_id") as! Int] = (price as AnyObject).object(forKey: "value") as? Double
        }
        return prices
    }

 
    
    
    static func getFuelType (completion: @escaping ([FuelStation]?)-> Void){
        
        var fuelTypes = [FuelType]()
        //VALIDAR SI EXISTE LA URL
        if let url = URL(string: "http://194.210.216.190/types") {
            //if let url = URL(string: "http://api.geonames.org/countryInfoJSON?formatted=true&lang=en&country=\(countryCode)&username=desenvolvimentoswift&style=full") {
            print("Inicia")
            let dataTask = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                print("Inicia 2")
                if data == nil {
                    completion(nil)
                    return
                }
                
                
    //               let resp = String(data:data!, encoding:String.Encoding.utf8)
      //              print(resp)
                    if let jsonDic = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String:Any] {
                        print("Inicia 3")
                        //                    print(data)
                        
                        for brand in (jsonDic["types"] as! [[String:Any]]) {
                            print("Inicia 4")
                            fuelTypes.append(
                                FuelType(
                                    id: brand["id"] as! Int,
                                    name: brand["value"] as! String
                                )
                            )
                            
                        }
                        /*
                        print(fuelTypes[0].id)
                        print(fuelTypes[0].name)*/
                        
                        
                    }
                    
                
                
                completion(nil)
            })
            dataTask.resume()
        } else {
            completion(nil)
        }
    }
    
    
    
    static func getBrands (completion: @escaping ([FuelStation]?)-> Void){
        
        var brands = [Brand]()
        //VALIDAR SI EXISTE LA URL
        if let url = URL(string: "http://194.210.216.190/brands") {
            //if let url = URL(string: "http://api.geonames.org/countryInfoJSON?formatted=true&lang=en&country=\(countryCode)&username=desenvolvimentoswift&style=full") {
            print("Inicia")
            let dataTask = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                print("Inicia 2")
                if data == nil {
                    completion(nil)
                    return
                }
                
                
            //        let resp = String(data:data!, encoding:String.Encoding.utf8)
              //      print(resp)
                    if let jsonDic = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String:Any] {
                        print("Inicia 3")
                        //                    print(data)
                        
                        for brand in (jsonDic["brands"] as! [[String:Any]]) {
                            let base64Image = (brand as AnyObject).object(forKey: "image") as! String
                            let decodedData = Data(base64Encoded: base64Image, options: NSData.Base64DecodingOptions.ignoreUnknownCharacters)
                            
                            brands.append(Brand(
                                    id: brand["id"] as! Int,
                                    name: brand["value"] as! String,
                                    image: UIImage(data: decodedData!)!
                                )
                            )

                            
                        }
                        
                         print(brands[0].id)
                         print(brands[0].name)
                        
                        
                    }
                    
               
                
                completion(nil)
            })
            dataTask.resume()
        } else {
            completion(nil)
        }
    }
    
    
    
    static func getDistrict (completion: @escaping ([FuelStation]?)-> Void){
        
        var districts = [District]()
        //VALIDAR SI EXISTE LA URL
        if let url = URL(string: "http://194.210.216.190/districts") {
            //if let url = URL(string: "http://api.geonames.org/countryInfoJSON?formatted=true&lang=en&country=\(countryCode)&username=desenvolvimentoswift&style=full") {
            print("Inicia")
            let dataTask = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                print("Inicia 2")
                if data == nil {
                    completion(nil)
                    return
                }
                
                
             //       let resp = String(data:data!, encoding:String.Encoding.utf8)
               //     print(resp)
                    if let jsonDic = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String:Any] {
                        print("Inicia 3")
                        //                    print(data)
                        
                        for district in (jsonDic["districts"] as! [[String:Any]]) {
                            print("Inicia 4")
                            districts.append(
                                District(
                                    id: district["id"] as! Int,
                                    name: district["value"] as! String
                                )
                            )
                            
                        }
                        
                         print(districts[0].id)
                         print(districts[0].name)
                        
                        
                    }
                    
                
                
                completion(nil)
            })
            dataTask.resume()
        } else {
            completion(nil)
        }
    }

    
    
    static func getMunicipalities (completion: @escaping ([FuelStation]?)-> Void){
        
        var municipalities = [Municipality]()
        //VALIDAR SI EXISTE LA URL
        if let url = URL(string: "http://194.210.216.190/municipalities") {
            //if let url = URL(string: "http://api.geonames.org/countryInfoJSON?formatted=true&lang=en&country=\(countryCode)&username=desenvolvimentoswift&style=full") {
            print("Inicia")
            let dataTask = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                print("Inicia 2")
                if data == nil {
                    completion(nil)
                    return
                }
               
            //        let resp = String(data:data!, encoding:String.Encoding.utf8)
              //      print(resp)
                    if let jsonDic = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String:Any] {
                        print("Inicia 3")
                        //                    print(data)
                        
                        for municipality in (jsonDic["municipalities"] as! [[String:Any]]) {
                            print("Inicia 4")
                            municipalities.append(
                                Municipality(
                                    id: municipality["id"] as! Int,
                                    name: municipality["value"] as! String,
                                    districtId:municipality["district_id"] as! Int
                                )
                            )
                            
                        }
                        
                        print(municipalities[0].id)
                        print(municipalities[0].districtId)
                        
                        
                    }
                    
               
                
                completion(nil)
            })
            dataTask.resume()
        } else {
            completion(nil)
        }
    }
    
}
