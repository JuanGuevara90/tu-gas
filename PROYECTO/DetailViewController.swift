//
//  DetailViewController.swift
//  TU GAS
//
//  Created by JUAN PABLO GUEVARA // FABIAN CONSTANTE on 26/12/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController, UISplitViewControllerDelegate {

    lazy var chartFrame: CGRect! = {
        CGRect(x: 0, y: 80, width: self.view.frame.size.width, height: self.view.frame.size.height - 80)
    }()
    
    var detailItem: Example? {
        didSet {
            self.configureView()
        }
    }
    var currentExampleController: UIViewController?
    
    func configureView() {
        
        if let example: Example = self.detailItem  {
            switch example {
            case .scroll:
                self.setSplitSwipeEnabled(true)
                self.showExampleController(Scroll())
            case .bars:
                self.setSplitSwipeEnabled(true)
                self.showExampleController(StackedBars())
            default :
                self.setSplitSwipeEnabled(true)
                self.showExampleController(GroupedBars())
            }
        }
    }
    
    fileprivate func showExampleController(_ controller: UIViewController) {
        if let currentExampleController = self.currentExampleController {
            currentExampleController.removeFromParentViewController()
            currentExampleController.view.removeFromSuperview()
        }
        self.addChildViewController(controller)
        self.view.addSubview(controller.view)
        self.currentExampleController = controller
    }
    
    fileprivate func setSplitSwipeEnabled(_ enabled: Bool) {
        if UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad {
            let splitViewController = UIApplication.shared.delegate?.window!!.rootViewController as! UISplitViewController
            splitViewController.presentsWithGesture = enabled
        }
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
