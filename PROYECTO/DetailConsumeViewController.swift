//
//  DetailConsumeViewController.swift
//  PROYECTO
//
//  Created by formando on 14/12/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import UIKit

class DetailConsumeViewController: UIViewController {

    @IBOutlet weak var imagen: UIImageView!
    
    var consume:Consume!
    override func viewDidLoad() {
        super.viewDidLoad()
        imagen.image=consume.brand?.image
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
       // print(consume.date)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
