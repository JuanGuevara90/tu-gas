//
//  FuelStationMapDetailTableViewCell.swift
//  TU GAS
//
//  Created by JUAN PABLO GUEVARA // FABIAN CONSTANTE on 29/12/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import UIKit
import MapKit

class FuelStationMapDetailTableViewCell: UITableViewCell, MKMapViewDelegate {

    @IBOutlet weak var fuelStationMapView: MKMapView!
    
    var latitude: Double!
    var longitude: Double!
    var brandName: String!
    var stationName: String!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setMapView() {
        let latDelta:CLLocationDegrees = 0.002
        let lonDelta:CLLocationDegrees = 0.002
        
        let span:MKCoordinateSpan = MKCoordinateSpanMake(latDelta, lonDelta)
        let currentLocation = CLLocationCoordinate2DMake(latitude, longitude)
        let region:MKCoordinateRegion = MKCoordinateRegionMake(currentLocation, span)
        fuelStationMapView.setRegion(region, animated: true)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = currentLocation
        annotation.title = brandName
        annotation.subtitle = stationName
        
        fuelStationMapView.addAnnotation(annotation)
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
