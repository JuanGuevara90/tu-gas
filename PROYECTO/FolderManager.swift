//
//  FolderManager.swift
//  TU GAS
//
//  Created by JUAN PABLO GUEVARA // FABIAN CONSTANTE  on 21/12/16.
//  Copyright © 2016 GuideTour. All rights reserved.
//

import Foundation

class FolderManager {
    
    // Returns True If Created, False OtherWise
    @discardableResult static func createDirectoryWithName(folderName: String) -> Bool {
        let fileManager = FileManager.default
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]).appending("/" + folderName)
        print(paths)
        if !fileManager.fileExists(atPath: paths) {
            try! fileManager.createDirectory(atPath: paths, withIntermediateDirectories: true, attributes: nil)
            return true
        } else {
            return false
        }
    }
    
    // Get Documents Path as String
    static func getDocumentsStringPath() -> String {
        return NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
    }
    
    static func getFavoriteURLFolderPath(favoriteId: String, folderName: String) -> URL {
        return FolderManager.getDocumentsURLPath().appendingPathComponent(favoriteId+"/"+folderName, isDirectory: true)
    }
    
    // Get Documents Path as URL
    static func getDocumentsURLPath() -> URL {
        return URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
    }
    
    static func listFilesAtPath(stringPath : String) {
        do {
            let fileManager = FileManager.default
            let filelist = try fileManager.contentsOfDirectory(atPath: getDocumentsStringPath() + "/" + stringPath)
            
            for filename in filelist {
                print(filename)
            }
        } catch let error {
            print("Error: \(error.localizedDescription)")
        }
    }
    
    @discardableResult static func deleteFolder(folderName: String) -> Bool {
        let fileManager = FileManager.default
        let paths = getDocumentsStringPath().appending("/" + folderName)
        
        if fileManager.fileExists(atPath: paths) {
            try! fileManager.removeItem(atPath: paths)
            return true
        } else {
            print("Something wrong")
            return false
        }
    }
    
    static func deleteFileFromFolder(filename: String, fileExtension: String) { // Eliminar archivo
        let fileManager = FileManager.default
        let fullPath = getDocumentsStringPath().appending( "/" + filename + "." + fileExtension)
        if fileManager.fileExists(atPath: fullPath) {
            try! fileManager.removeItem(atPath: fullPath)
        } else {
            print("Something wrong")
        }
    }
    
    static func getFilesFromPath(poiID : String, folderName: String) -> [String] {
        var array = [String]()
        do {
            let stringPath = getDocumentsStringPath().appending("/"+poiID+"/"+folderName)
            let filelist = try FileManager.default.contentsOfDirectory(atPath: stringPath)
            for filename in filelist {
                array.append((filename.components(separatedBy: ".")[0]))
            }
        } catch let error {
            print("Error: \(error.localizedDescription)")
        }
        return array
    }

}
