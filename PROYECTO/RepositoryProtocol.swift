//
//  RepositoryProtocol.swift
//  TU GAS
//
//  Created by JUAN PABLO GUEVARA // FABIAN CONSTANTE on 5/12/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import Foundation

protocol RepositoryProtocol {
    func save()
    func load()
}
