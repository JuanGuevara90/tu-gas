//
//  PriceConsumeTableViewCell.swift
//  PROYECTO
//
//  Created by JUAN PABLO GUEVARA on 30/12/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import UIKit

class PriceConsumeTableViewCell: UITableViewCell {

    @IBAction func edit(_ sender: Any) {
        if(totalText.text == ""){  // Campos con valor vacio
            print("Nilllll")
        }else{   // Campos con diferentes de vacio
            
            if(Double(totalText.text!) == nil){  // Campos direntes de Double
                UserDefaults.standard.set("",forKey: "totalPrice")
                totalText.text = ""
            }else{
                UserDefaults.standard.set(totalText.text ,forKey: "totalPrice")
            }
        }
    }
    
    @IBAction func begin(_ sender: Any) {
       
        let pre = UserDefaults.standard.double(forKey: "precioFuel")  // precio de combustible que elige el cliente
        if(totalText.text == ""){  // Campos con valor vacio
            print("Nilllll")
        }else{   // Campos con diferentes de vacio
            
            if(Double(totalText.text!) == nil){  // Campos direntes de Double
                UserDefaults.standard.set("",forKey: "totalPrice")
                totalText.text = ""
            }else{
                
                let valorPago:Double = Double(totalText.text!)!
                if (pre <= valorPago){
                    UserDefaults.standard.set(totalText.text ,forKey: "totalPrice")
                }else{
                    
                    UserDefaults.standard.set("" ,forKey: "totalPrice")
                    totalText.placeholder = "Pay > Or = \(pre) €"
                    totalText.text = ""
                }
                
                
            }
        }
    }
    
    @IBAction func end(_ sender: Any) {
        let precioFuel = UserDefaults.standard.double(forKey: "precioFuel")  // precio de combustible que elige el cliente
        if(totalText.text == ""){  // Campos con valor vacio
            print("Nilllll")
        }else{   // Campos con diferentes de vacio
            
            if(Double(totalText.text!) == nil){  // Campos direntes de Double
                UserDefaults.standard.set("",forKey: "totalPrice")
                totalText.text = ""
            }else{
                
                let valorPago:Double = Double(totalText.text!)!
                if (precioFuel <= valorPago){
                    UserDefaults.standard.set(totalText.text ,forKey: "totalPrice")
                }else{
                    
                    UserDefaults.standard.set("" ,forKey: "totalPrice")
                    totalText.placeholder = "Pay > Or = \(precioFuel) €"
                    totalText.text = ""
                }
                
                
            }
        }

    }
    
    @IBOutlet weak var totalText: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
