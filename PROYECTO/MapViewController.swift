//
//  MapViewController.swift
//  TU GAS
//
//  Created by JUAN PABLO GUEVARA  //  FABIAN CONSTANTE  on 28/12/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import UIKit
import MapKit
class MapViewController: UIViewController {

    var latitude: Double!
    var longitude: Double!
    
    var fuelStations:[FuelStation]!
    var brands:[Brand]!
    
    @IBOutlet weak var fuelStationMapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dataLoad()
        loadMapView()
        // Do any additional setup after loading the view.
    }
    
    func dataLoad(){
        self.brands = Brand.loadAll()
    }
    
    func loadMapView() {
        let latDelta:CLLocationDegrees = 0.05
        let lonDelta:CLLocationDegrees = 0.05
    
        let span:MKCoordinateSpan = MKCoordinateSpanMake(latDelta , lonDelta)
        let currentLocation = CLLocationCoordinate2DMake(39.74953310000001, -8.807682999999997)  // Ubicacion centro de Leiria
        let region:MKCoordinateRegion = MKCoordinateRegionMake(currentLocation, span)
        fuelStationMapView.setRegion(region, animated: true)
        
        for c in fuelStations {
            let annotation = MKPointAnnotation()
            let currentLocation = CLLocationCoordinate2DMake(c.latitude, c.longitude)
            annotation.coordinate = currentLocation
            
            for bra in brands {
                if(c.brandId == bra.id){
                    annotation.title = bra.name
                }
            }
            
             annotation.subtitle = c.title
            fuelStationMapView.addAnnotation(annotation)

        }

        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backAction(_ sender: Any) {
         self.dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
