//
//  OrderStations.swift
//  PROYECTO
//
//  Created by JUAN PABLO GUEVARA on 10/12/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import Foundation

class OrderStations:NSObject, NSCoding {
    
    var fuelStation:FuelStation?
    var distancia:Double?
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(fuelStation, forKey: "fuelStation")
        aCoder.encode(distancia, forKey: "distancia")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fuelStation = aDecoder.decodeObject(forKey: "fuelStation") as? FuelStation
        distancia = aDecoder.decodeObject(forKey: "distancia") as? Double
    }
    
    init(fuelStation:FuelStation, distancia:Double) {
        self.fuelStation = fuelStation
        self.distancia = distancia
    }
    
}
