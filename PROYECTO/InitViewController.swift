//
//  InitViewController.swift
//  TU GAS
//
//  Created by JUAN PABLO GUEVARA // FABIAN CONSTANTE on 19/11/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import UIKit
import SystemConfiguration

class InitViewController: UITabBarController {

    
    var brands:[Brand]!
    var fuelStations:[FuelStation]!
    var districts:[District]!
    var municipalities:[Municipality]!
    var fuelTypes:[FuelType]!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(isInternetAvailable()){
            FolderManager.deleteFileFromFolder(filename: "FuelStations", fileExtension: "data")
            FolderManager.deleteFileFromFolder(filename: "FuelTypes", fileExtension: "data")
            FolderManager.deleteFileFromFolder(filename: "Brands", fileExtension: "data")
            FolderManager.deleteFileFromFolder(filename: "Districts", fileExtension: "data")
            FolderManager.deleteFileFromFolder(filename: "Municipalities", fileExtension: "data")
           getWSData()
        }
     ///   si tiene internet  guarda o actualiza al archivo
        
      //  sino haga otra cosa no internet usa lo q tiene en el archivo
        
       
        // Do any additional setup after loading the view.
    }

    func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       // print(districts.count)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
    
     func getWSData() {

         if FuelStation.loadAll().count == 0 {
            WebServiceClient.getStations(){ (stations) in
                self.fuelStations = stations!
                if (FuelStation.saveMany(self.fuelStations))
                {
                    print("OKay")
                }else{
                    print("Error")
                }
                
            }
        }
        if Brand.loadAll().count == 0 {
            WebServiceClient.getBrands(){ (brands) in
                self.brands = brands!
                if (Brand.saveMany(self.brands))
                {
                    print("OKay")
                }else{
                    print("Error")
                }
                
            }
        }
        
        if FuelType.loadAll().count == 0 {
            WebServiceClient.getFuelType(){ (fuelTypes) in
                self.fuelTypes = fuelTypes!
                if (FuelType.saveMany(self.fuelTypes))
                {
                    print("OKay")
                }else{
                    print("Error")
                }
                
            }
        }
        
        if Municipality.loadAll().count == 0 {
            WebServiceClient.getMunicipalities(){ (municipalities) in
                self.municipalities = municipalities!
                if (Municipality.saveMany(self.municipalities))
                {
                    print("OKay")
                }else{
                    print("Error")
                }
            }
        }
        
        if District.loadAll().count == 0 {
            WebServiceClient.getDistrict(){ (districts) in
                self.districts = districts!
                if (District.saveMany(self.districts))
                {
                    print("OKay")
                }else{
                    print("Error")
                }
            }
        }
    }
    
    /*
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   */
    
}
