//
//  District.swift
//  TU GAS
//
//  Created by JUAN PABLO GUEVARA // FABIAN CONSTANTE on 18/11/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import Foundation

class District: NSObject, NSCoding {
    var id: Int!
    var name: String!
    var municipalityId: Int!
    
    init(id: Int, name: String) {
        self.id = id
        self.name = name
    }
    
    struct Const {
        static let id = "id"
        static let name = "name"
    }
    
    // MARK: NSCoding
    func encode(with coder: NSCoder) {
        coder.encode(id, forKey: Const.id)
        coder.encode(name, forKey: Const.name)
    }
    
    required init?(coder decoder: NSCoder) {
        id = decoder.decodeObject(forKey: Const.id) as! Int
        name = decoder.decodeObject(forKey: Const.name) as! String
    }
    
    static func saveMany (_ brands: [District]) -> Bool {
        let documentsPath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        
        let filePath = documentsPath.appendingPathComponent("Districts.data")
        print(filePath)
        let path = filePath.path
        
        
        if NSKeyedArchiver.archiveRootObject(brands, toFile: path) {
            print("Successfully saved District")
            return true
        }else{
            print("Successfully saved District")
            return false
        }
        
        
    }
    
    static func loadAll()  -> [District] {
        var dataToRetrieve = [District]()
        
        let documentsPath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        let filePath = documentsPath.appendingPathComponent("Districts.data", isDirectory: false)
        
        let path = filePath.path
        
        if let newData = NSKeyedUnarchiver.unarchiveObject(withFile: path) as? [District] {
            dataToRetrieve = newData
        }
        return dataToRetrieve
    }
}
