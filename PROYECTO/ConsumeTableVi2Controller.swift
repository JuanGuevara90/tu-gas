//
//  ConsumeTableVi2Controller.swift
//  PROYECTO
//
//  Created by JUAN PABLO GUEVARA on 5/1/17.
//  Copyright © 2017 Ipleiria. All rights reserved.
//

import UIKit

class ConsumeTableVi2Controller: UITableViewController {

    var fuelStation: FuelStation!
    var brand: Brand!
    var district: District!
    var municipality: Municipality!
    var fuelType: FuelType!
    var precio:Double!
    
    var mileaString = ""
    var totalPay = ""
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        UserDefaults.standard.set("" ,forKey: "mileage")
        UserDefaults.standard.set("",forKey: "totalPrice")
        UserDefaults.standard.set(self.precio ,forKey: "precioFuel")
        fechaMod.val = "" 

    }

    @IBAction func cancelAction(_ sender: Any) {
        UserDefaults.standard.set("" ,forKey: "mileage")
        UserDefaults.standard.set("",forKey: "totalPrice")
        UserDefaults.standard.set("",forKey: "precioFuel")
        fechaMod.val = ""
          dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveAction(_ sender: Any) {
        mileaString = UserDefaults.standard.string(forKey: "mileage")!
        totalPay = UserDefaults.standard.string(forKey: "totalPrice")!

        if(mileaString == "" && totalPay == ""){  // Campos con valor vacio
            UserDefaults.standard.set("" ,forKey: "mileage")
            UserDefaults.standard.set("",forKey: "totalPrice")
            UserDefaults.standard.set(self.precio ,forKey: "precioFuel")
            fechaMod.val = ""
            print("Nilllll")
        }else{   // Campos con diferentes de vacio
            
            if(Double(mileaString) == nil  || Double(totalPay) == nil){  // Campos direntes de Double
                UserDefaults.standard.set("" ,forKey: "mileage")
                UserDefaults.standard.set("",forKey: "totalPrice")
                UserDefaults.standard.set(self.precio ,forKey: "precioFuel")
                fechaMod.val = ""
                print("no es double")
            }else{          // Campos con tipo de Dato Double
                
                if(fechaMod.val == ""){  // Cuando no cambia la fecha
                    let f = Date()
                    let newConsume = Consume(fuelStation: fuelStation, precioFuel: precio, date: f, mileage: Double(mileaString)!, totalPay: Double(totalPay)!, brand: brand,fuelType:fuelType)
                    ConsumeRepository.repository.consumes.append(newConsume)
                    ConsumeRepository.repository.save()
                    UserDefaults.standard.set("" ,forKey: "mileage")
                    UserDefaults.standard.set("",forKey: "totalPrice")
                    fechaMod.val = ""
                    self.dismiss(animated: true, completion: nil)
                }else{  // Cuando cambia la fecha
                    let newConsume = Consume(fuelStation: fuelStation, precioFuel: precio, date: fechaMod.fec, mileage: Double(mileaString)!, totalPay: Double(totalPay)!, brand: brand,fuelType:fuelType)
                    ConsumeRepository.repository.consumes.append(newConsume)
                    ConsumeRepository.repository.save()
                    UserDefaults.standard.set("" ,forKey: "mileage")
                    UserDefaults.standard.set("",forKey: "totalPrice")
                    fechaMod.val = ""
                    //self.dismiss(animated: true, completion: nil)
                    navigationController?.popViewController(animated: true)
                }
                
               
            }
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    func validateMileage() -> Double{
        let validate:Double = 0
        
        if (ConsumeRepository.repository.consumes.count == 0){ // Por primera vez valor de distancia
            return validate
        }else{ // Para una proxima vez
            let tam = ConsumeRepository.repository.consumes.count
            return ConsumeRepository.repository.consumes[tam-1].mileage!
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        switch (section) {
        case 0:
            return "Brand"
        case 1:
            return "Date"
        case 2:
           let value = String(format:"%.0f", (ConsumeRepository.repository.consumes[ConsumeRepository.repository.consumes.count - 1].mileage)!)
            return "Last Mileage " + value + " km"
        default :
            return "Total to pay"
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 4
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch (indexPath.section, indexPath.row) {
        case (0, 0):
            let cell = tableView.dequeueReusableCell(withIdentifier: "imagen") as! ImagenConsume2TableViewCell
            return cell.frame.height
        case (1, _):
            let cell = tableView.dequeueReusableCell(withIdentifier: "date") as! DateConsume2TableViewCell
            return cell.frame.height
        case (2, 0) :
            let cell = tableView.dequeueReusableCell(withIdentifier: "mileage") as! MileageConsume2TableViewCell
            return cell.frame.height
            
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "price") as! PriceConsume2TableViewCell
            return cell.frame.height
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch (indexPath.section, indexPath.row) {
        case (0, 0):
            let cell = tableView.dequeueReusableCell(withIdentifier: "imagen", for: indexPath) as! ImagenConsume2TableViewCell
            return cell
        case (1, _):
            let cell = tableView.dequeueReusableCell(withIdentifier: "date", for: indexPath) as! DateConsume2TableViewCell
            return cell
        case (2, 0) :
            let cell = tableView.dequeueReusableCell(withIdentifier: "mileage", for: indexPath) as! MileageConsume2TableViewCell
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "price", for: indexPath) as! PriceConsume2TableViewCell
            return cell
        }
    }
    
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if let result = cell as? ImagenConsume2TableViewCell {
          result.imagenConsume.image = self.brand.image
            
        }
        
    }
    
  
}
