//
//  ImagenConsumeTableViewCell.swift
//  PROYECTO
//
//  Created by JUAN PABLO GUEVARA on 30/12/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import UIKit

class ImagenConsumeTableViewCell: UITableViewCell {

    @IBOutlet weak var imagenConsume: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
