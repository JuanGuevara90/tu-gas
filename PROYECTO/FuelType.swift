//
//  FuelType.swift
//  TU GAS
//
//  Created by JUAN PABLO GUEVARA  // FABIAN CONSTANTE on 18/11/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import Foundation

class FuelType: NSObject, NSCoding {
    var id: Int!
    var name: String!
    var selected: Bool!
    
    init(id: Int, name: String) {
        self.id = id
        self.name = name
        self.selected = true
    }
    
    struct Const {
        static let id = "id"
        static let name = "name"
        static let selected = "selected"
    }
    
    // MARK: NSCoding
    func encode(with coder: NSCoder) {
        coder.encode(id, forKey: Const.id)
        coder.encode(name, forKey: Const.name)
        coder.encode(selected, forKey: Const.selected)
    }
    
    required init?(coder decoder: NSCoder) {
        id = decoder.decodeObject(forKey: Const.id) as! Int
        name = decoder.decodeObject(forKey: Const.name) as! String
        selected = decoder.decodeObject(forKey: Const.selected) as! Bool
    }
    
    static func saveMany (_ brands: [FuelType]) -> Bool {
        let documentsPath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        
        let filePath = documentsPath.appendingPathComponent("FuelTypes.data")
        print(filePath)
        let path = filePath.path
        
        if NSKeyedArchiver.archiveRootObject(brands, toFile: path) {
            return true
        }
        
        return false
    }
    
    static func loadAll() -> [FuelType] {
        var dataToRetrieve = [FuelType]()
        
        let documentsPath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        let filePath = documentsPath.appendingPathComponent("FuelTypes.data", isDirectory: false)
        
        let path = filePath.path
        
        if let newData = NSKeyedUnarchiver.unarchiveObject(withFile: path) as? [FuelType] {
            dataToRetrieve = newData
        }
        return dataToRetrieve
    }
}
