//
//  Scroll.swift
//  TU GAS
//
//  Created by JUAN PABLO GUEVARA // FABIAN CONSTANTE on 26/12/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//


import UIKit
import SwiftCharts

class Scroll: UIViewController {
    
    fileprivate var chart: Chart? // arc
    
    var consumes:[Consume]! // Todos los consumos
   
    override func viewDidLoad() {
        super.viewDidLoad()
        cargaGra()
    }
    
    
    func cargaGra()->(){
        self.tabBarController?.tabBar.isHidden = true
        let labelSettings = ChartLabelSettings(font: ExamplesDefaults.labelFont)
        
        self.consumes=ConsumeRepository.repository.consumes  // Carga todos los consumos
        
        var datos = LoadData.data.cargar1(c: self.consumes)  // Cargar datos a presentar en el grafico
        
        var chartPoints0:[ChartPoint] = [ChartPoint]()
        var chartPoints1:[ChartPoint] = [ChartPoint]()
        
        chartPoints0.append(self.createChartPoint(0,0,labelSettings))
        chartPoints1.append(self.createChartPoint(0,0,labelSettings))
        
        var aux:Double! = 0
        for i in  0 ..< datos.count
        {
            
            chartPoints0.append(self.createChartPoint(Double(i+1),datos[i].0,labelSettings))
            chartPoints1.append(self.createChartPoint(Double(i+1),datos[i].1,labelSettings))
            if(datos[i].0 > aux){
                aux = datos[i].0
            }
            if(datos[i].1 > aux){
                aux = datos[i].1
            }
        }
        
        
        
        
        let xValues = stride(from: 0, through: 31, by: 1).map {ChartAxisValueDouble(Double($0), labelSettings: labelSettings)}
        /*  let yValues = ChartAxisValuesGenerator.generateYAxisValuesWithChartPoints(chartPoints0, minSegmentCount: 10, maxSegmentCount: 20, multiple: 2, axisValueGenerator: {ChartAxisValueDouble($0, labelSettings: labelSettings)}, addPaddingSegmentIfEdge: false)*/
        let yValues = stride(from: 0, through: aux + 50, by: aux / 5 ).map {ChartAxisValueDouble(Double($0), labelSettings: labelSettings)}
        
        let xModel = ChartAxisModel(axisValues: xValues, axisTitleLabel: ChartAxisLabel(text: "Days of the current month", settings: labelSettings))
        let yModel = ChartAxisModel(axisValues: yValues, axisTitleLabel: ChartAxisLabel(text: "Total Pay (RED)  / Total Distance (BLUE)", settings: labelSettings.defaultVertical()))
        let scrollViewFrame = ExamplesDefaults.chartFrame(self.view.bounds)
        //  let chartFrame = CGRect(x: 0, y: 0, width: 1400, height: scrollViewFrame.size.height)
        let chartFrame = CGRect(x: 0, y: 0, width: 1400, height: 610) // Cambio de altura para la imagen a 610
        
        // calculate coords space in the background to keep UI smooth
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async {
            let coordsSpace = ChartCoordsSpaceLeftBottomSingleAxis(chartSettings: ExamplesDefaults.chartSettings, chartFrame: chartFrame, xModel: xModel, yModel: yModel)
            
            DispatchQueue.main.async {
                let (xAxis, yAxis, innerFrame) = (coordsSpace.xAxis, coordsSpace.yAxis, coordsSpace.chartInnerFrame)
                
                let lineModel0 = ChartLineModel(chartPoints: chartPoints0, lineColor: UIColor.red, animDuration: 1, animDelay: 0)
                let lineModel1 = ChartLineModel(chartPoints: chartPoints1, lineColor: UIColor.blue, animDuration: 1, animDelay: 0, dashPattern: [5,10])
                let chartPointsLineLayer = ChartPointsLineLayer(xAxis: xAxis, yAxis: yAxis, innerFrame: innerFrame, lineModels: [lineModel0, lineModel1])
                
                let settings = ChartGuideLinesDottedLayerSettings(linesColor: UIColor.black, linesWidth: ExamplesDefaults.guidelinesWidth)
                let guidelinesLayer = ChartGuideLinesDottedLayer(xAxis: xAxis, yAxis: yAxis, innerFrame: innerFrame, settings: settings)
                
                let scrollView = UIScrollView(frame: scrollViewFrame)
                scrollView.contentSize = CGSize(width: chartFrame.size.width, height: scrollViewFrame.size.height)
                //        self.automaticallyAdjustsScrollViewInsets = false // nested view controller - this is in parent
                
                let chart = Chart(
                    frame: chartFrame,
                    layers: [
                        xAxis,
                        yAxis,
                        guidelinesLayer,
                        chartPointsLineLayer
                    ]
                )
                
                scrollView.addSubview(chart.view)
                self.view.addSubview(scrollView)
                self.chart = chart
                
            }
        }
    }
    
    
    fileprivate func createChartPoint(_ x: Double, _ y: Double, _ labelSettings: ChartLabelSettings) -> ChartPoint {
        return ChartPoint(x: ChartAxisValueDouble(x, labelSettings: labelSettings), y: ChartAxisValueDouble(y))
    }
}
