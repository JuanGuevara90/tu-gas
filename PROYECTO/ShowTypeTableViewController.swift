//
//  ShowTypeTableViewController.swift
//  TU GAS
//
//  Created by JUAN PABLO GUEVARA  // FABIAN CONSTANTE  on 2/12/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import UIKit

class ShowTypeTableViewController: UITableViewController {

    var option:Int!
    var fuelTypes:[FuelType]!
    var typeDistance:[TypeDistance]=[TypeDistance(id: 0, valor: "10 km"),TypeDistance(id: 1, valor: "20 km"),TypeDistance(id: 2, valor: "50 km"),TypeDistance(id: 3, valor: "All")]
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        
    }
    func loadData(){
        self.fuelTypes = FuelType.loadAll()
        OperationQueue.main.addOperation({ () -> Void in
            self.tableView.reloadData()
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch (option) {
        case (0):
             return fuelTypes.count
        default:
            return typeDistance.count
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let valor = UserDefaults.standard.integer(forKey: "key")
        let valor2 = UserDefaults.standard.integer(forKey: "key2")
        
        switch option {
        case (0):
            if valor==fuelTypes[indexPath.row].id  {
                let cell = tableView.dequeueReusableCell(withIdentifier: "type", for: indexPath) as! TypeTableViewCell
                cell.typeLabel.text = fuelTypes[indexPath.row].name
                cell.accessoryType = UITableViewCellAccessoryType.checkmark
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "type", for: indexPath) as! TypeTableViewCell
                cell.typeLabel.text = fuelTypes[indexPath.row].name
                cell.accessoryType = UITableViewCellAccessoryType.none
                return cell
            }
            
        default:
            if valor2==typeDistance[indexPath.row].id {
                let cell = tableView.dequeueReusableCell(withIdentifier: "type", for: indexPath) as! TypeTableViewCell
                cell.typeLabel.text = typeDistance[indexPath.row].valor
                cell.accessoryType = UITableViewCellAccessoryType.checkmark
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "type", for: indexPath) as! TypeTableViewCell
                cell.typeLabel.text = typeDistance[indexPath.row].valor
                cell.accessoryType = UITableViewCellAccessoryType.none
                return cell
            }
        }
    }
    
    
 
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        let valor = UserDefaults.standard.integer(forKey: "key")
        let valor2 = UserDefaults.standard.integer(forKey: "key2")
      
        switch option {
        case (0):  // Tipo Gasolina
            if valor==fuelTypes[indexPath.row].id {
                tableView.reloadData()
                let cell = tableView.dequeueReusableCell(withIdentifier: "type", for: indexPath) as! TypeTableViewCell
                cell.typeLabel.text = fuelTypes[indexPath.row].name
                cell.accessoryType = UITableViewCellAccessoryType.none
            }else{
                tableView.reloadData()
                let cell = tableView.dequeueReusableCell(withIdentifier: "type", for: indexPath) as! TypeTableViewCell
                cell.typeLabel.text = fuelTypes[indexPath.row].name
                cell.accessoryType = UITableViewCellAccessoryType.checkmark
                UserDefaults.standard.set(fuelTypes[indexPath.row].id, forKey: "key")
                ArchivingRepository.repository.userConf[0].valor=fuelTypes[indexPath.row].id
                ArchivingRepository.repository.save()
            }
            
            
            
        default:
            if valor2==typeDistance[indexPath.row].id {
                tableView.reloadData()
                let cell = tableView.dequeueReusableCell(withIdentifier: "type", for: indexPath) as! TypeTableViewCell
                cell.typeLabel.text = typeDistance[indexPath.row].valor
                cell.accessoryType = UITableViewCellAccessoryType.none
            }else{
                tableView.reloadData()
                let cell = tableView.dequeueReusableCell(withIdentifier: "type", for: indexPath) as! TypeTableViewCell
                cell.typeLabel.text = typeDistance[indexPath.row].valor
                cell.accessoryType = UITableViewCellAccessoryType.checkmark
                UserDefaults.standard.set(typeDistance[indexPath.row].id, forKey: "key2")
                ArchivingRepository.repository.userConf[1].valor=typeDistance[indexPath.row].id
                ArchivingRepository.repository.save()
            }
        }
        
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
