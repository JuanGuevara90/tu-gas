//
//  FuelStationLocationTableViewCell.swift
//  TU GAS
//
//  Created by JUAN PABLO GUEVARA // FABIAN CONSTANTE on 1/12/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import UIKit
import MapKit

class FuelStationLocationTableViewCell: UITableViewCell {

    @IBOutlet weak var districtLabel: UILabel!
    
    @IBOutlet weak var distanceLabel: UILabel!
   
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
