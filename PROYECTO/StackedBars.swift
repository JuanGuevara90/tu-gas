//
//  StackedBars.swift
//  TU GAS
//
//  Created by JUAN PABLO GUEVARA // FABIAN CONSTANTE on 26/12/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import UIKit
import SwiftCharts

class StackedBars: UIViewController {

    fileprivate var chart: Chart? // arc
    
    let sideSelectorHeight: CGFloat = 50
    
    var consumes:[Consume]! // Todos los consumos
    
    func chart(horizontal: Bool) -> Chart {
        let labelSettings = ChartLabelSettings(font: ExamplesDefaults.labelFont)
        
        let color0 = UIColor.gray.withAlphaComponent(0.6)
        let color1 = UIColor.blue.withAlphaComponent(0.6)
        let color2 = UIColor.red.withAlphaComponent(0.6)
        let color3 = UIColor.green.withAlphaComponent(0.6)
        let color4 = UIColor.brown.withAlphaComponent(0.6)
        
        let zero = ChartAxisValueDouble(0)
      
        var datos = LoadData.data.cargar2(c: self.consumes)  // Carga de Datos
    
        var barModels:[ChartStackedBarModel] = [ChartStackedBarModel]()
        
        var aux:Double = 0
        for i in  0 ..< datos.count
        {
            barModels.append(ChartStackedBarModel(constant: ChartAxisValueString(LoadData.data.getMonth(month: datos[i].0), order: i+1, labelSettings: labelSettings), start: zero, items: [
                ChartStackedBarItemModel(quantity: datos[i].1, bgColor: color0),
                ChartStackedBarItemModel(quantity: datos[i].2, bgColor: color1),
                ChartStackedBarItemModel(quantity: datos[i].3, bgColor: color2),
                ChartStackedBarItemModel(quantity: datos[i].4, bgColor: color3),
                ChartStackedBarItemModel(quantity: datos[i].5, bgColor: color4)
                ]))
            
            if(datos[i].1 + datos[i].2 + datos[i].3 + datos[i].4 + datos[i].5 > aux){
                aux = datos[i].1 + datos[i].2 + datos[i].3 + datos[i].4 + datos[i].5
            }
        }
     
        
        let (axisValues1, axisValues2) = (
            stride(from: 0, through: aux + 50, by: aux / 4).map {ChartAxisValueDouble(Double($0), labelSettings: labelSettings)},
            [ChartAxisValueString("", order: 0, labelSettings: labelSettings)] + barModels.map{$0.constant} + [ChartAxisValueString("", order: 5, labelSettings: labelSettings)]
        )
        let (xValues, yValues) = horizontal ? (axisValues1, axisValues2) : (axisValues2, axisValues1)
        
        let xModel = ChartAxisModel(axisValues: xValues, axisTitleLabel: ChartAxisLabel(text: "Months", settings: labelSettings))
        let yModel = ChartAxisModel(axisValues: yValues, axisTitleLabel: ChartAxisLabel(text: "Weeks:  First(Gray) / Second (Blue) / Third (Red) / Fourth (Green) / Fiveth (Brown)", settings: labelSettings.defaultVertical()))
        
        let frame = ExamplesDefaults.chartFrame(self.view.bounds)
        let chartFrame = self.chart?.frame ?? CGRect(x: frame.origin.x, y: frame.origin.y, width: frame.size.width, height: frame.size.height - sideSelectorHeight)
        let coordsSpace = ChartCoordsSpaceLeftBottomSingleAxis(chartSettings: ExamplesDefaults.chartSettings, chartFrame: chartFrame, xModel: xModel, yModel: yModel)
        let (xAxis, yAxis, innerFrame) = (coordsSpace.xAxis, coordsSpace.yAxis, coordsSpace.chartInnerFrame)
        
        let chartStackedBarsLayer = ChartStackedBarsLayer(xAxis: xAxis, yAxis: yAxis, innerFrame: innerFrame, barModels: barModels, horizontal: horizontal, barWidth: 40, animDuration: 0.5)
        
        let settings = ChartGuideLinesDottedLayerSettings(linesColor: UIColor.black, linesWidth: ExamplesDefaults.guidelinesWidth)
        let guidelinesLayer = ChartGuideLinesDottedLayer(xAxis: xAxis, yAxis: yAxis, innerFrame: innerFrame, settings: settings)
        
        return Chart(
            frame: chartFrame,
            layers: [
                xAxis,
                yAxis,
                guidelinesLayer,
                chartStackedBarsLayer
            ]
        )
    }
    
    func showChart(horizontal: Bool) {
        self.chart?.clearView()
        
        let chart = self.chart(horizontal: horizontal)
        self.view.addSubview(chart.view)
        self.chart = chart
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.consumes=ConsumeRepository.repository.consumes  // Carga de Consumos totales en el arreglo 
        self.showChart(horizontal: false)
        if let chart = self.chart {
            let sideSelector = DirSelector(frame: CGRect(x: 0, y: chart.frame.origin.y + chart.frame.size.height, width: self.view.frame.size.width, height: self.sideSelectorHeight), controller: self)
            self.view.addSubview(sideSelector)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.consumes=ConsumeRepository.repository.consumes  // Carga de Consumos totales en el arreglo
        self.showChart(horizontal: false)
        if let chart = self.chart {
            let sideSelector = DirSelector(frame: CGRect(x: 0, y: chart.frame.origin.y + chart.frame.size.height, width: self.view.frame.size.width, height: self.sideSelectorHeight), controller: self)
            self.view.addSubview(sideSelector)
        }
        
    }
    
    class DirSelector: UIView {
        
        let horizontal: UIButton
        let vertical: UIButton
        
        weak var controller: StackedBars?
        
        fileprivate let buttonDirs: [UIButton : Bool]
        
        init(frame: CGRect, controller: StackedBars) {
            
            self.controller = controller
            
            self.horizontal = UIButton()
            self.horizontal.setTitle("Horizontal", for: UIControlState())
            self.vertical = UIButton()
            self.vertical.setTitle("Vertical", for: UIControlState())
            
            self.buttonDirs = [self.horizontal : true, self.vertical : false]
            
            super.init(frame: frame)
            
            self.addSubview(self.horizontal)
            self.addSubview(self.vertical)
            
            for button in [self.horizontal, self.vertical] {
                button.titleLabel?.font = ExamplesDefaults.fontWithSize(14)
                button.setTitleColor(UIColor.blue, for: UIControlState())
                button.addTarget(self, action: #selector(DirSelector.buttonTapped(_:)), for: .touchUpInside)
            }
        }
        
        func buttonTapped(_ sender: UIButton) {
            let horizontal = sender == self.horizontal ? true : false
            controller?.showChart(horizontal: horizontal)
        }
        
        override func didMoveToSuperview() {
            let views = [self.horizontal, self.vertical]
            for v in views {
                v.translatesAutoresizingMaskIntoConstraints = false
            }
            
            let namedViews = views.enumerated().map{index, view in
                ("v\(index)", view)
            }
            
            var viewsDict = Dictionary<String, UIView>()
            for namedView in namedViews {
                viewsDict[namedView.0] = namedView.1
            }
            
            let buttonsSpace: CGFloat = Env.iPad ? 20 : 10
            
            let hConstraintStr = namedViews.reduce("H:|") {str, tuple in
                "\(str)-(\(buttonsSpace))-[\(tuple.0)]"
            }
            
            let vConstraits = namedViews.flatMap {NSLayoutConstraint.constraints(withVisualFormat: "V:|[\($0.0)]", options: NSLayoutFormatOptions(), metrics: nil, views: viewsDict)}
            
            self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: hConstraintStr, options: NSLayoutFormatOptions(), metrics: nil, views: viewsDict)
                + vConstraits)
        }
        
        required init(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}
