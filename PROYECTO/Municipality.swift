//
//  Municipality.swift
//  TU GAS
//
//  Created by JUAN PABLO GUEVARA  // FABIAN CONSTANTE on 18/11/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import Foundation


class Municipality: NSObject, NSCoding {
    var id: Int!
    var name: String!
    var districtId: Int!
    
    init(id: Int, name: String, districtId: Int) {
        self.id = id
        self.name = name
        self.districtId = districtId
    }
    
    struct Const {
        static let id = "id"
        static let name = "name"
        static let districtId = "districtId"
    }
    
    // MARK: NSCoding
    func encode(with coder: NSCoder) {
        coder.encode(id, forKey: Const.id)
        coder.encode(name, forKey: Const.name)
        coder.encode(districtId, forKey: Const.districtId)
    }
    
    required init?(coder decoder: NSCoder) {
        id = decoder.decodeObject(forKey: Const.id) as! Int
        name = decoder.decodeObject(forKey: Const.name) as! String
        districtId = decoder.decodeObject(forKey: Const.districtId) as! Int
    }
    
    static func saveMany (_ brands: [Municipality]) -> Bool {
        let documentsPath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        
        let filePath = documentsPath.appendingPathComponent("Municipalities.data")
        print(filePath)
        let path = filePath.path
        
        if NSKeyedArchiver.archiveRootObject(brands, toFile: path) {
            return true
        }
        
        return false
    }
    
    static func loadAll() -> [Municipality] {
        var dataToRetrieve = [Municipality]()
        
        let documentsPath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        let filePath = documentsPath.appendingPathComponent("Municipalities.data", isDirectory: false)
        
        let path = filePath.path
        
        if let newData = NSKeyedUnarchiver.unarchiveObject(withFile: path) as? [Municipality] {
            dataToRetrieve = newData
        }
        return dataToRetrieve
    }
    
    
    
}
