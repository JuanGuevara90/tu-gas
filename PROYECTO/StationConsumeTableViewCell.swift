//
//  StationConsumeTableViewCell.swift
//  PROYECTO
//
//  Created by JUAN PABLO GUEVARA on 5/1/17.
//  Copyright © 2017 Ipleiria. All rights reserved.
//

import UIKit

class StationConsumeTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var priceLabel: UILabel!
    
    @IBOutlet weak var imagen: UIImageView!
    
    func setFuelStation (_ fuelStation: FuelStation, brands: [Brand]) {
        self.titleLabel.text = fuelStation.title
        var value:String=""
        let valor = UserDefaults.standard.integer(forKey: "key")
        for fuel in fuelStation.prices{
            if(fuel.key == valor){
                value="\(fuel.value) €"
                self.priceLabel.text = value  // Valor de Gasolina
            }
        }
        
        for brand in brands {
            if(brand.id == fuelStation.brandId) {
                self.imagen.image = brand.image
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
