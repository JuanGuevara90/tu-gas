//
//  MultiplicationTableViewController.swift
//  TU GAS
//
//  Created by JUAN PABLO GUEVARA  // FABIAN CONSTANTE on 20/12/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import UIKit
import GlyuckDataGrid
class ReportTableViewController: UIViewController, DataGridViewDataSource {

    @IBOutlet weak var dataGridView: DataGridView!
    var opTab:Int! // 0) Op = 0 tabla 1   1) Op = 1 tabla 2  2)  Op = 2  tabla 3
    
    var cabc1:[String] = ["Days","Coste Total by Day / €","Distance Total by Day / km"]
    var cabc2:[String] = ["Mouth","First Week / €","Second Week / €","Third Week / €","Fourth Week / €","Fiveth Week / €"]
    var cabc3:[String] = ["Month","Distance / km","Costes / €"]
    
    var detalle1:[(Double,Double)]!
    var detalle2:[(Int,Double,Double,Double,Double,Double)]!
    var detalle3:[(Int,Double,Double)]!
    
    let date = Date()  //Fecha de Hoy
    var consumes:[Consume]! // Todos los consumos
   
    
    static override func initialize() {
        super.initialize()
        
        let dataGridAppearance = DataGridView.glyuck_appearanceWhenContained(in: self)!
        dataGridAppearance.row1BackgroundColor = nil
        dataGridAppearance.row2BackgroundColor = nil
        
        let cornerHeaderAppearance = DataGridViewCornerHeaderCell.glyuck_appearanceWhenContained(in: self)!
        cornerHeaderAppearance.backgroundColor = UIColor.white
        cornerHeaderAppearance.borderBottomWidth = 1 / UIScreen.main.scale
        cornerHeaderAppearance.borderBottomColor = UIColor(white: 0.73, alpha: 1)
        cornerHeaderAppearance.borderRightWidth = 1 / UIScreen.main.scale
        cornerHeaderAppearance.borderRightColor = UIColor(white: 0.73, alpha: 1)
        
        let rowHeaderAppearance = DataGridViewRowHeaderCell.glyuck_appearanceWhenContained(in: self)!
        rowHeaderAppearance.backgroundColor = UIColor(white: 0.95, alpha: 1)
        rowHeaderAppearance.borderBottomWidth = 1 / UIScreen.main.scale
        rowHeaderAppearance.borderBottomColor = UIColor(white: 0.73, alpha: 1)
        
        let columnHeaderAppearance = DataGridViewColumnHeaderCell.glyuck_appearanceWhenContained(in: self)!
        columnHeaderAppearance.borderRightWidth = 1 / UIScreen.main.scale
        columnHeaderAppearance.borderRightColor = UIColor(white: 0.73, alpha: 1)
        
        let cellAppearance = DataGridViewContentCell.glyuck_appearanceWhenContained(in: self)!
        cellAppearance.borderRightWidth = 1 / UIScreen.main.scale
        cellAppearance.borderRightColor = UIColor(white: 0.73, alpha: 1)
        cellAppearance.borderBottomWidth = 1 / UIScreen.main.scale
        cellAppearance.borderBottomColor = UIColor(white: 0.73, alpha: 1)
        
        columnHeaderAppearance.backgroundColor = UIColor(white: 0.95, alpha: 1)
        let labelAppearance = UILabel.glyuck_appearanceWhenContained(in: self)!
        labelAppearance.appearanceFont = UIFont.systemFont(ofSize: 12, weight: UIFontWeightLight)
        labelAppearance.appearanceTextAlignment = .center
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()
        self.consumes = [Consume]()
        self.consumes=ConsumeRepository.repository.consumes
        
        switch opTab {
        case 0:
           self.detalle1 = LoadData.data.cargar1(c: self.consumes)
        case 1:
            self.detalle2 = LoadData.data.cargar2(c: self.consumes)
        default:
            self.detalle3 = LoadData.data.cargar3(c: self.consumes)
        }
        dataGridView.dataSource = self
       /*
        dataGridView.rowHeaderWidth = 80
        dataGridView.columnHeaderHeight = 30
         */
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func numberOfColumnsInDataGridView(_ dataGridView: DataGridView) -> Int {
        switch opTab {
        case 0:
            return 3
        case 1:
            return 6
        default:
            return 3
        }
    }
    
    // And number of rows
    func numberOfRowsInDataGridView(_ dataGridView: DataGridView) -> Int {
        switch opTab {
        case 0:
            return detalle1.count  // 31
        case 1:
            return 3  // 3 por mes anterio mes actual es proyeccion
        default:
            return 3
        }
    }
    
    // Then you'll need to provide titles for columns headers
    func dataGridView(_ dataGridView: DataGridView, titleForHeaderForRow row: Int) -> String {
       //  return String(row + 10) // No hace nada
        return ""
    }
    
    // And rows headers
    func dataGridView(_ dataGridView: DataGridView, titleForHeaderForColumn column: Int) -> String {
        switch opTab {
        case 0:
            return cabc1[column]
        case 1:
            return cabc2[column]
        default:
            return cabc3[column]
        }
       // return String(column + 1) /// Cabecera de tabla
    }
    
    // And for text for content cells
    func dataGridView(_ dataGridView: DataGridView, textForCellAtIndexPath indexPath: IndexPath) -> String {
       // Fila
        switch opTab {
        case 0:  // Opcion  de tabla
            switch indexPath.dataGridColumn {  // Columna
            case 0:
                return String( (indexPath.dataGridRow + 1) * (indexPath.dataGridColumn + 1) )
            case 1:
                return String(detalle1[indexPath.dataGridRow].0)
            default:
                return String(detalle1[indexPath.dataGridRow].1)
            }
        case 1:
            switch indexPath.dataGridColumn {  // Columna
            case 0:
                return LoadData.data.getMonth(month: detalle2[indexPath.dataGridRow].0)
            case 1:
                return String(detalle2[indexPath.dataGridRow].1)
            case 2:
                return String(detalle2[indexPath.dataGridRow].2)
            case 3:
                return String(detalle2[indexPath.dataGridRow].3)
            case 4:
                return String(detalle2[indexPath.dataGridRow].4)
            default:
                return String(detalle2[indexPath.dataGridRow].5)
            }
        default:
            switch indexPath.dataGridColumn {  // Columna
            case 0:
                return LoadData.data.getMonth(month: detalle3[indexPath.dataGridRow].0)
            case 1:
                return String(detalle3[indexPath.dataGridRow].1)
            default:
                return String(detalle3[indexPath.dataGridRow].2)
            }
        }
       // return String( (indexPath.dataGridRow + 1) * (indexPath.dataGridColumn + 1) )

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
