//
//  Consume.swift
//  TU GAS
//
//  Created by JUAN PABLO GUEVARA // FABIAN CONSTANTE  on 2/12/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import Foundation

import UIKit

class Consume: NSObject, NSCoding {

    var fuelStation:FuelStation?
    var precioFuel:Double?
    var date:Date?
    var mileage:Double?
    var totalPay:Double?
    var brand:Brand?
    var fuelType:FuelType?
        
    func encode(with aCoder: NSCoder) {
        aCoder.encode(fuelStation, forKey: "fuelStation")
        aCoder.encode(precioFuel, forKey: "precioFuel")
        aCoder.encode(date, forKey: "date")
        aCoder.encode(mileage, forKey: "mileage")
        aCoder.encode(totalPay, forKey: "totalPay")
        aCoder.encode(brand, forKey: "brand")
        aCoder.encode(fuelType, forKey: "fuelType")
    }
        
    required init?(coder aDecoder: NSCoder) {
        fuelStation = aDecoder.decodeObject(forKey: "fuelStation") as? FuelStation
        precioFuel = aDecoder.decodeObject(forKey: "precioFuel") as? Double
        date = aDecoder.decodeObject(forKey: "date") as? Date
        mileage = aDecoder.decodeObject(forKey: "mileage") as? Double
        totalPay = aDecoder.decodeObject(forKey: "totalPay") as? Double
        brand = aDecoder.decodeObject(forKey: "brand") as? Brand
        fuelType = aDecoder.decodeObject(forKey: "fuelType") as? FuelType
    }
    
    init(fuelStation:FuelStation, precioFuel:Double,date:Date,mileage:Double,totalPay:Double,brand:Brand,fuelType:FuelType) {
        self.fuelStation = fuelStation
        self.precioFuel = precioFuel
        self.date=date
        self.mileage=mileage
        self.totalPay=totalPay
        self.brand=brand
        self.fuelType=fuelType
    }
    
}
