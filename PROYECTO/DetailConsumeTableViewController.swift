//
//  DetailConsumeTableViewController.swift
//  TU GAS
//
//  Created by JUAN PABLO GUEVARA  //  FABIAN CONSTANTE   on 17/12/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
class DetailConsumeTableViewController: UITableViewController,CLLocationManagerDelegate {

    var consume:Consume!
    let locationManager = CLLocationManager()
    var currentLocation = CLLocationCoordinate2D()
     var distance = "..."
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // locationInit()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 4
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch (section) {
        case 0:
            return "Main Information"
        case 1:
            return "Fuel Information"
        case 2:
            return "Consume"
        default :
        return ""
        }
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch (indexPath.section, indexPath.row) {
        case (0, 0):
            let cell = tableView.dequeueReusableCell(withIdentifier: "mainInformation") as! MainInformationTableViewCell
            return cell.frame.height
        case (1, _):
            let cell = tableView.dequeueReusableCell(withIdentifier: "fuelInformation") as! FuelInformationTableViewCell
            return cell.frame.height
        case (2, 0):
            let cell = tableView.dequeueReusableCell(withIdentifier: "consumeInformation") as! ConsumeInformationTableViewCell
            return cell.frame.height
            
        default: //= case (2, 1):
            let cell = tableView.dequeueReusableCell(withIdentifier: "fuelStationMap") as! FuelStationMapDetailTableViewCell
            return cell.frame.height
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch (indexPath.section, indexPath.row) {
        case (0, 0):
            let cell = tableView.dequeueReusableCell(withIdentifier: "mainInformation", for: indexPath) as! MainInformationTableViewCell
            return cell
        case (1, _):
            let cell = tableView.dequeueReusableCell(withIdentifier: "fuelInformation", for: indexPath) as! FuelInformationTableViewCell
            return cell
        case (2, 0):
            let cell = tableView.dequeueReusableCell(withIdentifier: "consumeInformation", for: indexPath) as! ConsumeInformationTableViewCell
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "fuelStationMap", for: indexPath) as! FuelStationMapDetailTableViewCell
            return cell
            
        }
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        var value:String=""
        var value2:String=""
      
        if let result = cell as? FuelStationMapDetailTableViewCell {
            result.latitude = consume.fuelStation?.latitude
            result.longitude = consume.fuelStation?.longitude
            result.stationName = consume.fuelStation?.title
            result.brandName = consume.brand?.name
            result.setMapView()
            
        } else if let result = cell as? MainInformationTableViewCell {
            result.imagen.image=consume.brand?.image
            result.addressLabel.text = consume.fuelStation?.address
            result.branLabel.text = consume.brand?.name
            result.nameLabel.text = consume.fuelStation?.title
            
            let formatter = DateFormatter()
            formatter.dateFormat = "E MMM dd, yyyy"
            let val = formatter.string(from: consume.date!)
            result.dateLabel.text = val
            
        } else if let result = cell as? FuelInformationTableViewCell {
            result.nameLabel.text = consume.fuelType?.name
            value = String(format:"%.2f", consume.precioFuel!)
            value2="\(value) €"
            result.priceLabel.text = value2
        }else if let result = cell as? ConsumeInformationTableViewCell {
            value = String(format:"%.2f", consume.totalPay!)
            value2="\(value) €"
            result.totalLabel.text = value2
            
            let mile = String(format:"%.2f", consume.mileage!)
            let mile2 = "\(mile) km"
            result.mileageLabel.text = mile2
        }
    }
    
    

    
    
    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
