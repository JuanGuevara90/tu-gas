//
//  UserDefaul.swift
//  TU GAS
//
//  Created by JUAN PABLO GUEVARA // FABIAN CONSTANTE on 7/12/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import Foundation

class UserDefaul:NSObject, NSCoding {
    
    var key:String?
    var valor:Int?
    
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(key, forKey: "key")
        aCoder.encode(valor, forKey: "valor")
    }
    
    required init?(coder aDecoder: NSCoder) {
        key = aDecoder.decodeObject(forKey: "key") as? String
        valor = aDecoder.decodeObject(forKey: "valor") as? Int
    }
    
    init(key:String, valor:Int) {
        self.key = key
        self.valor = valor
    }
    

    
}
