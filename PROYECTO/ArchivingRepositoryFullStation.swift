//
//  ArchivingRepositoryFullStation.swift
//  PROYECTO
//
//  Created by JUAN PABLO GUEVARA on 27/11/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import Foundation

class ArchivingRepositoryFullStation:RepositoryProtocol {
    
    //singleton repository
    static let repository = ArchivingRepositoryFullStation() //shared Instance
    fileprivate init () {}
    
    var fuelStation = [FuelStation]()
    
    var documentsPath = URL(fileURLWithPath: "")
    var filePath = URL(fileURLWithPath: "")
    
    func save() {
        documentsPath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) [0])
        print(documentsPath)
        filePath = documentsPath.appendingPathComponent("people.data", isDirectory: false)
        print(filePath)
        
        let path = filePath.path
        
        if NSKeyedArchiver.archiveRootObject(fuelStation, toFile: path) {
            print("Successfully saved people")
        } else {
            print("Failure saving people")
        }
    }
    
    func load() {
        documentsPath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) [0])
        print(documentsPath)
        filePath = documentsPath.appendingPathComponent("people.data", isDirectory: false)
        print(filePath)
        
        let path = filePath.path
        
        if let newData = NSKeyedUnarchiver.unarchiveObject(withFile: path) as? [FuelStation]{
            fuelStation = newData
        }
        
    }
}
