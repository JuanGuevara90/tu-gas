//
//  WebServiceClient.swift
//  TU GAS
//
//  Created by JUAN PABLO GUEVARA // FABIAN CONSTANTE on 19/11/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import Foundation
import UIKit

class WebServiceClient{
    
    
    static func getStations (completion: @escaping ([FuelStation]?)-> Void) {
        var fuelStations = [FuelStation]()
        if let url = URL(string: "http://194.210.216.190/smartgas/index.php/stations") {
            let dataTask = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                if data == nil {
                    completion(fuelStations)
                    return
                }
                if let jsonDic = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String:Any] {
                        for station in (jsonDic["stations"] as! [[String:Any]]) {
                            let prices = getPrices(station["prices"] as! [[String : Any]])
                            fuelStations.append(
                                FuelStation(
                                    title: station["title"] as! String,
                                    address: station["address"] as! String,
                                    latitude: station["latitude"] as! Double,
                                    longitude: station["longitude"] as! Double,
                                    brandId: station["brand_id"] as! Int,
                                    districtId: station["district_id"] as! Int,
                                    municipalityId: station["municipality_id"] as! Int,
                                    prices: prices!,
                                    distance: 0
                                )
                            )
                        }
                        completion(fuelStations)
                        return
                    }
            })
            dataTask.resume()
        } else {
            completion(fuelStations)
        }
    }
    
    
    static func getPrices(_ stationPrices: [[String:Any]]) -> [Int:Double]! {
        var prices = [Int:Double]()
        for price in stationPrices {
            prices[(price as AnyObject).object(forKey: "type_id") as! Int] = (price as AnyObject).object(forKey: "value") as? Double
        }
        return prices
    }
    
    
    
    
    static func getFuelType (completion: @escaping ([FuelType]?)-> Void){
        
        var fuelTypes = [FuelType]()
        if let url = URL(string: "http://194.210.216.190/smartgas/index.php/types") {
            let dataTask = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                if data == nil {
                    completion(fuelTypes)
                    return
                }
                if let jsonDic = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String:Any] {
                        for brand in (jsonDic["types"] as! [[String:Any]]) {
                            fuelTypes.append(
                                FuelType(
                                    id: brand["id"] as! Int,
                                    name: brand["value"] as! String
                                )
                            )
                        }
                    completion(fuelTypes)
                    return
                }
            })
            dataTask.resume()
        } else {
            completion(fuelTypes)
        }
    }
    
    
    
    static func getBrands (completion: @escaping ([Brand]?)-> Void){
        
        var brands = [Brand]()
        if let url = URL(string: "http://194.210.216.190/smartgas/index.php/brands") {
            let dataTask = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                if data == nil {
                    completion(brands)
                    return
                }
                if let jsonDic = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String:Any] {
                        for brand in (jsonDic["brands"] as! [[String:Any]]) {
                            let base64Image = (brand as AnyObject).object(forKey: "image") as! String
                            let decodedData = Data(base64Encoded: base64Image, options: NSData.Base64DecodingOptions.ignoreUnknownCharacters)
                            
                            brands.append(Brand(
                                id: brand["id"] as! Int,
                                name: brand["value"] as! String,
                                image: UIImage(data: decodedData!)!
                                )
                            )
                        }
                        completion(brands)
                         return
                }
                
            })
            dataTask.resume()
        } else {
            completion(brands)
        }
    }
    
   
    static func getDistrict(completion: @escaping ([District]?)-> Void){
        
        var districts = [District]()
        
        if let url = URL(string: "http://194.210.216.190/smartgas/index.php/districts") {
            let dataTask = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                if data == nil {
                    print("retunr nil")
                    completion(districts)
                    return
                }
                if let jsonDic = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String:Any] {
                        for district in (jsonDic["districts"] as! [[String:Any]]) {
                            districts.append(
                                District(
                                    id: district["id"] as! Int,
                                    name: district["value"] as! String
                                )
                            )
                        }
                        completion(districts)
                        return
                }
            })
            dataTask.resume()
        }else{
            completion(districts)
        }
    }
    
    static func getMunicipalities (completion: @escaping ([Municipality]?)-> Void){
        
        var municipalities = [Municipality]()

        if let url = URL(string: "http://194.210.216.190/smartgas/index.php/municipalities") {
            let dataTask = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                if data == nil {
                    completion(municipalities)
                    return
                }
                if let jsonDic = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String:Any] {
                        for municipality in (jsonDic["municipalities"] as! [[String:Any]]) {
                            municipalities.append(
                                Municipality(
                                    id: municipality["id"] as! Int,
                                    name: municipality["value"] as! String,
                                    districtId:municipality["district_id"] as! Int
                                )
                            )
                            
                        }
                    completion(municipalities)
                    return
                }
            })
            dataTask.resume()
        } else {
            completion(municipalities)
        }
    }
}
