//
//  AppDelegate.swift
//  PROYECTO
//
//  Created by formando on 17/11/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        ConsumeRepository.repository.load()
        
        ArchivingRepository.repository.load()
        if ArchivingRepository.repository.userConf.count == 0 {  // Configuracion inicia
            let newUser = UserDefaul(key: "key", valor: 6)  // Creo el objeto
            ArchivingRepository.repository.userConf.append(newUser)  // guardo el arreglo
            ArchivingRepository.repository.save()  // Guardo archiving
            
            let newUser2 = UserDefaul(key: "key2", valor: 3)
            ArchivingRepository.repository.userConf.append(newUser2)
            ArchivingRepository.repository.save()
            
            UserDefaults.standard.set(newUser.valor, forKey: newUser.key!)  // Memoria UserDefaults
            UserDefaults.standard.set(newUser2.valor, forKey: newUser2.key!)  // Memmoria UserDefaults

        }else{
            UserDefaults.standard.set(ArchivingRepository.repository.userConf[0].valor, forKey: ArchivingRepository.repository.userConf[0].key!)
            UserDefaults.standard.set(ArchivingRepository.repository.userConf[1].valor!, forKey: ArchivingRepository.repository.userConf[1].key!)
        }
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

