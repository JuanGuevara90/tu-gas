//
//  ConsumeStationsTableViewController.swift
//  PROYECTO
//
//  Created by JUAN PABLO GUEVARA on 5/1/17.
//  Copyright © 2017 Ipleiria. All rights reserved.
//

import UIKit
import CoreLocation
import Darwin
import MapKit

class ConsumeStationsTableViewController: UITableViewController, UISearchBarDelegate , CLLocationManagerDelegate {

    var tableRows = 0
    var brands:[Brand]!
    var fuelStations:[FuelStation]!
    var districts=[District]()
    var municipalities:[Municipality]!
    var fuelTypes:[FuelType]!
    var latitude:Double!
    var longitude:Double!
    
    var locationManager = CLLocationManager()
    var currentLocation: CLLocation!
     var typeDistance:[Double]=[10,20,50,999]
     let showFuelStationSegueIdentifier = "showFuelStation"
    var op:Bool = false
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
         var op:Bool = false
        fuelStationLoad()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.distanceFilter = kCLDistanceFilterNone
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
            
        }
        else {
            print("Error")
        }
        self.tableView.reloadData()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func viewWillAppear(_ animated: Bool) {
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
         var op:Bool = false
        fuelStationLoad()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.distanceFilter = kCLDistanceFilterNone
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
            
        }
        else {
            print("Error")
        }
        self.tableView.reloadData()
    }

    
    @IBAction func backAction(_ sender: Any) {
          dismiss(animated: true, completion: nil)
    }
    
    // ver boton cancel ....
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    // ocultar cancel
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
    }
    
    // ocular keyboard al dar click en cancelar
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
    }

    
    func fuelStationLoad(){
        self.brands = Brand.loadAll()
        self.districts = District.loadAll()
        self.municipalities = Municipality.loadAll()
        self.fuelTypes = FuelType.loadAll()
        let fuelStationsMod = FuelStation.loadAll()
        
        if CLLocationManager.locationServicesEnabled() {
            // Cargo arreglo fuelStantion
            var fuelStationsNew = [FuelStation]()  // Arreglo para filtrar por distancia
            // Obtengo valor de Configuraciones distancia
            let valor3 = UserDefaults.standard.integer(forKey: "key2")
            
            if latitude == nil && longitude == nil {
                fuelStations = [FuelStation]()
            }else{
                for stations in fuelStationsMod {
                    let distance = calculateDistance(lat1: latitude, lon1: longitude, lat2: stations.latitude, lon2: stations.longitude)
                    if typeDistance[valor3] != 999
                    {
                        if distance <= typeDistance[valor3]
                        {
                            stations.distance=distance
                            fuelStationsNew.append(stations)  // Almacenamiento de Distancia <= al parametro de Settings
                        }
                    }else
                    {
                        stations.distance=distance
                        fuelStationsNew.append(stations)    // Almacenamiento de Distancia si = All como parametro en Settings
                    }
                }
                if fuelStationsNew.count==0{  // Cuando las distacia es mayor al de settings y no se guarda en fuelStationsNew
                    fuelStations = [FuelStation]()
                    brands = [Brand]()
                    districts = [District]()
                    municipalities = [Municipality]()
                }else{
                    // self.fuelStations = fuelStationsNew
                    // Filtrar por tipo
                    
                    // Valor de Settings tipo de gasolina
                    let valor = UserDefaults.standard.integer(forKey: "key")  // valor recupera de settings para el tipo de gasolina
                    var fuelStationsTypeNew = [FuelStation]()
                    for fuelSta in fuelStationsNew{
                        for type in fuelSta.prices
                        {
                            if(type.key == valor){
                                fuelStationsTypeNew.append(fuelSta)
                            }
                        }
                    }
                    if(fuelStationsTypeNew.count == 0){ // Cuando no hay gasolineras con ese tipo de gasolina
                        fuelStations = [FuelStation]()
                        brands = [Brand]()
                        districts = [District]()
                        municipalities = [Municipality]()
                    }else{
                        // Ordenar por el tipo de Valor de gasolina que esta en settings
                        if(op == false){ // Primera vez
                            self.fuelStations = orderArray(arr: fuelStationsTypeNew, op: 1)
                            
                            self.brands = Brand.loadAll()
                            self.districts = District.loadAll()
                            self.municipalities = Municipality.loadAll()
                            self.fuelTypes = FuelType.loadAll()
                            self.brands = Brand.loadAll()
                            op = true
                        }else{ // Segunda vez
                            
                            for i in  0 ..< fuelStations.count
                            {
                                let dis =  calculateDistance(lat1: latitude, lon1: longitude, lat2: fuelStations[i].latitude, lon2: fuelStations[i].longitude)
                                fuelStations[i].distance = dis
                            }
                            
                            self.brands = Brand.loadAll()
                            self.districts = District.loadAll()
                            self.municipalities = Municipality.loadAll()
                            self.fuelTypes = FuelType.loadAll()
                            self.brands = Brand.loadAll()
                        }
                        
                    }
                }
            }
        }else{
            self.fuelStations = [FuelStation]()
            self.brands = [Brand]()
            self.districts = [District]()
            self.municipalities = [Municipality]()
        }
        
        OperationQueue.main.addOperation({ () -> Void in
            self.tableView.reloadData()
        })
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if let stations = fuelStations {
            for station in stations {
                
                for bran in brands{
                    if station.brandId == bran.id{
                        let text = bran.name
                        if text?.lowercased().range(of: searchText.lowercased()) != nil || searchText == "" {
                            station.active = true
                        }else{
                            station.active = false
                        }
                    }
                }
                
            }
            
            OperationQueue.main.addOperation({ () -> Void in
                self.tableView.reloadData()
            })
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableRows = 0
        if let stations = fuelStations {
            for station in stations {
                if station.active == true {
                    tableRows += 1
                }
            }
        }
        return tableRows
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "station", for: indexPath) as! StationConsumeTableViewCell
        var cellIndex = 0
        if let stations = self.fuelStations {
            for station in stations {
                if cellIndex == indexPath.row {
                    cell.setFuelStation(station, brands: brands)
                }
                if station.active == true {
                    cellIndex += 1
                }
            }
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let fuelStation = fuelStations![indexPath.row]
        performSegue(withIdentifier: showFuelStationSegueIdentifier, sender: fuelStation)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == showFuelStationSegueIdentifier {     // Informacion enviar la informacion a  ShowFuelStationTableViewController
            if let fullStationViewController = segue.destination as? ConsumeTableVi2Controller {
                if let fuelStation = sender as? FuelStation {
                    fullStationViewController.fuelStation = fuelStation
                    
                    let valor = UserDefaults.standard.integer(forKey: "key")
                    for fuel in fuelStation.prices{
                        if(fuel.key == valor){
                           fullStationViewController.precio = fuel.value
                        }
                    }
                    
                    for brand in brands {
                        if brand.id == fuelStation.brandId {
                            fullStationViewController.brand = brand
                        }
                    }
                    for district in districts {
                        if district.id == fuelStation.districtId {
                            fullStationViewController.district = district
                        }
                    }
                    for municipality in municipalities {
                        if municipality.id == fuelStation.municipalityId {
                            fullStationViewController.municipality = municipality
                        }
                    }
                    
                    
                    for fuelType in fuelTypes {
                        if  fuelType.id == valor{
                            fullStationViewController.fuelType = fuelType
                        }
                    }
                    
                }
            }
            
        }   //  Enviar la informacion a  MapViewController
       
    }
    
        func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        // print("Erro: \(error)")
        
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.startUpdatingLocation()
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {  // Obtiene la latitud y longitud actual del dispositivo
        
        let locationObj = locations.last
        let coord = locationObj?.coordinate
        if let c = coord {
            self.latitude = c.latitude   // Latitud Actual
            self.longitude = c.longitude   // Longitud Actual
        }
        
        fuelStationLoad()
        
       
        self.tableView.reloadData()
        
    }
    
    
    
    func orderArray (arr:[FuelStation], op: Int)->[FuelStation]{
        
        var arreglo:[FuelStation]=arr
        
        // Op = 0  Se ordena por distancia
        if(op == 0)
        {
            for _ in  0 ..< arreglo.count - 1
            {
                for j in  0 ..< arreglo.count - 1
                {
                    if (arreglo[j].distance! > arreglo[j + 1].distance!)
                    {
                        let tmp:FuelStation = arreglo[j+1]
                        arreglo[j+1] = arreglo[j]
                        arreglo[j] = tmp
                    }
                }
            }
        }else
        { // Op diferente Se ordena por type de gasolina
            let valor = UserDefaults.standard.integer(forKey: "key")
            var ini:Double = 0
            var des:Double = 0
            for _ in  0 ..< arreglo.count - 1
            {
                for j in  0 ..< arreglo.count - 1
                {
                    for price in arreglo[j].prices{
                        if (price.key == valor){
                            ini = price.value  /// Valor de precio
                        }
                    }
                    for price2 in arreglo[j + 1].prices{
                        if (price2.key == valor){
                            des = price2.value  // Valor de precio mas uno posicion del arreglo
                        }
                    }
                    if (ini > des)
                    {
                        let tmp:FuelStation = arreglo[j+1]
                        arreglo[j+1] = arreglo[j]
                        arreglo[j] = tmp
                    }
                    
                }
            }
            
        }
        
        
        return arreglo
    }
    
    func toRad (valor: Double)->Double{  // Calculo de Radianes
        return valor * M_PI / 180
    }
    
    func calculateDistance(lat1: Double,lon1:Double,lat2:Double,lon2:Double) ->Double{  // Calculo de Distancia de Haversine
        let radio:Double = 6371
        
        let diflat = toRad(valor: lat2 - lat1)
        let diflon = toRad(valor: lon2 - lon1)
        let a1 = sin(diflat/2) * sin(diflat/2)
        
        let a2 = sin(diflon/2) * sin(diflon/2) * cos(toRad(valor: lat1)) * cos(toRad(valor: lat2))
        
        let a = a1+a2
        let c = 2 * asin(sqrt(a))
        let d =  radio * c
        return d
        
        
    }
    
}
