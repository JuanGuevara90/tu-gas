//
//  MileageConsume2TableViewCell.swift
//  PROYECTO
//
//  Created by JUAN PABLO GUEVARA on 5/1/17.
//  Copyright © 2017 Ipleiria. All rights reserved.
//

import UIKit

class MileageConsume2TableViewCell: UITableViewCell {

    @IBOutlet weak var mileageText: UITextField!
    
    func validateMileage() -> Double{
        let validate:Double = 0
        
        if (ConsumeRepository.repository.consumes.count == 0){ // Por primera vez valor de distancia
            return validate
        }else{ // Para una proxima vez
            let tam = ConsumeRepository.repository.consumes.count
            return ConsumeRepository.repository.consumes[tam-1].mileage!
        }
    }
    
    @IBAction func edit(_ sender: Any) {
        if(mileageText.text == ""){  // Campos con valor vacio
            print("Nilllll")
        }else{   // Campos con diferentes de vacio
            
            if(Double(mileageText.text!) == nil){  // Campos direntes de Double
                UserDefaults.standard.set("",forKey: "mileage")
                mileageText.text = ""
            }else{
                UserDefaults.standard.set(mileageText.text ,forKey: "mileage")
            }
        }
    }
    
    @IBAction func begin(_ sender: Any) {
        if(mileageText.text == ""){  // Campos con valor vacio
            print("Nilllll")
        }else{   // Campos con diferentes de vacio
            
            if(Double(mileageText.text!) == nil){  // Campos direntes de Double
                UserDefaults.standard.set("",forKey: "mileage")
                mileageText.text = ""
            }else{
                let valide = validateMileage()
                let valorIngresado = Double(mileageText.text!)
                if(valide < valorIngresado!){
                    // La informacion ingresada por el usuario esta correcta
                    UserDefaults.standard.set(mileageText.text ,forKey: "mileage")
                }else{
                    // La informacion ingresada por el usuario esta incorrecta
                    UserDefaults.standard.set("",forKey: "mileage")
                    mileageText.placeholder = "Mileage > \(valide) km"
                    mileageText.text = ""
                    
                }
                
            }
        }
    }
    
    @IBAction func end(_ sender: Any) {
        if(mileageText.text == ""){  // Campos con valor vacio
            print("Nilllll")
        }else{   // Campos con diferentes de vacio
            
            if(Double(mileageText.text!) == nil){  // Campos direntes de Double
                UserDefaults.standard.set("",forKey: "mileage")
                mileageText.text = ""
            }else{
                let valide = validateMileage()
                let valorIngresado = Double(mileageText.text!)
                if(valide < valorIngresado!){
                    // La informacion ingresada por el usuario esta correcta
                    UserDefaults.standard.set(mileageText.text ,forKey: "mileage")
                }else{
                    // La informacion ingresada por el usuario esta incorrecta
                    UserDefaults.standard.set("",forKey: "mileage")
                    mileageText.placeholder = "Mileage > \(valide) km"
                    mileageText.text = ""
                }
                
            }
        }

    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
