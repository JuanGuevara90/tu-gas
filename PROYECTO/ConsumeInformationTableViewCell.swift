//
//  ConsumeInformationTableViewCell.swift
//  TU GAS
//
//  Created by JUAN PABLO GUEVARA // FABIAN CONSTANTE on 17/12/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import UIKit

class ConsumeInformationTableViewCell: UITableViewCell {
    @IBOutlet weak var mileageLabel: UILabel!

    @IBOutlet weak var totalLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
