//
//  Vehicle.swift
//  PROYECTO
//
//  Created by JUAN PABLO GUEVARA on 1/12/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import Foundation
import UIKit

class Vehicle: NSObject, NSCoding {
    var make: String
    var model: String
    var mileageKm: Float?
    var consume: Float?
    var image: UIImage?
    var fuelId: Int?
    
    init(make: String, model: String, image: UIImage, fuelId: Int){
        self.make = make
        self.model = model
        self.image = image
        self.fuelId = fuelId
    }
    
    struct Const {
        static let make = "make"
        static let model = "model"
        static let mileageKm = "mileageKm"
        static let consume = "consume"
        static let image = "image"
        static let fuelId = "fuelId"
    }
    
    // MARK: NSCoding
    func encode(with coder: NSCoder) {
        coder.encode(make, forKey: Const.make)
        coder.encode(model, forKey: Const.model)
        coder.encode(mileageKm ?? 0.0, forKey: Const.mileageKm)
        coder.encode(consume ?? 0.0, forKey: Const.consume)
        coder.encode(image, forKey: Const.image)
        coder.encode(fuelId ?? 0, forKey: Const.fuelId)
    }
    
    required init?(coder decoder: NSCoder) {
        self.make = decoder.decodeObject(forKey: Const.make) as! String
        self.model = decoder.decodeObject(forKey: Const.model) as! String
        self.mileageKm = decoder.decodeObject(forKey: Const.mileageKm) as? Float ?? 0.0
        self.consume = decoder.decodeObject(forKey: Const.consume) as? Float ?? 0.0
        self.fuelId = decoder.decodeInteger(forKey: Const.fuelId)
        self.image = decoder.decodeObject(forKey: Const.image) as? UIImage
    }
    
    static func saveMany (_ brands: [Vehicle]) -> Bool {
        let documentsPath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        
        let filePath = documentsPath.appendingPathComponent("Vehicles.data")
        
        let path = filePath.path
        
        if NSKeyedArchiver.archiveRootObject(brands, toFile: path) {
            return true
        }
        
        return false
    }
    
    static func loadAll() -> [Vehicle] {
        var dataToRetrieve = [Vehicle]()
        
        let documentsPath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        let filePath = documentsPath.appendingPathComponent("Vehicles.data", isDirectory: false)
        
        let path = filePath.path
        
        if let newData = NSKeyedUnarchiver.unarchiveObject(withFile: path) as? [Vehicle] {
            dataToRetrieve = newData
        }
        return dataToRetrieve
    }
    
    
}
