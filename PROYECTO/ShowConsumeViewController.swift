//
//  ShowConsumeViewController.swift
//  TU GAS
//
//  Created by JUAN PABLO GUEVARA //  FABIAN CONSTANTE  on 13/12/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import UIKit

class ShowConsumeViewController: UIViewController , UITableViewDelegate, UITableViewDataSource {

    
    var detail:Consume!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var barOptions: UISegmentedControl!
    @IBOutlet weak var miBoton: UIButton!
    
    
    var consumes:[Consume]!
    
    let showDetailConsumeSegueIdentifier = "detailConsume"
    
    override func viewDidLoad() {
        super.viewDidLoad()
       /* self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "consumeCell")
*/
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.consumes=ConsumeRepository.repository.consumes
        self.tableView.reloadData()
    }
    
    
    @IBAction func selecOption(_ sender: AnyObject) {
        
        switch barOptions.selectedSegmentIndex {
        case 0:
            self.tableView.isHidden = false
            self.miBoton.isHidden = true
            self.miBoton.titleLabel?.text="Hola 2"
        case 1:
            self.tableView.isHidden = true
            self.miBoton.isHidden = false
        default:
            break;
        }
    }
    
    /*
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    */
    
    override func viewDidAppear(_ animated: Bool) {
        self.tableView.reloadData()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.consumes.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "consumeCell", for: indexPath) as! ConsumeTableViewCell
        cell.labelStation.text = consumes[indexPath.row].brand?.name
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        let result = formatter.string(from: consumes[indexPath.row].date!)
        cell.labelDate.text = result// Configure the cell...
        
        return cell
    }

    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
          detail = self.consumes[indexPath.row]
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //performSegue(withIdentifier: showDetailConsumeSegueIdentifier, sender: self.consumes[indexPath.row])
      
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == showDetailConsumeSegueIdentifier {
            if let detailViewController = segue.destination as? DetailConsumeTableViewController {
                detailViewController.consume = detail
            }
        }
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
