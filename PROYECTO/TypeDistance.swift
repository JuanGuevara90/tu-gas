//
//  TypeDistance.swift
//  TU GAS
//
//  Created by JUAN PABLO GUEVARA // FABIAN CONSTANTE on 7/12/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import Foundation

class TypeDistance:NSObject, NSCoding {
    
    var id:Int?
    var valor:String?
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(valor, forKey: "valor")
    }
    
    required init?(coder aDecoder: NSCoder) {
        id = aDecoder.decodeObject(forKey: "id") as? Int
        valor = aDecoder.decodeObject(forKey: "valor") as? String
    }
    
    init(id:Int, valor:String) {
        self.id = id
        self.valor = valor
    }
    
}
