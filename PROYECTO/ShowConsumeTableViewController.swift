//
//  ShowConsumeTableViewController.swift
//  TU GAS
//
//  Created by JUAN PABLO GUEVARA //  FABIAN COSNTANTEon 26/12/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import UIKit

class ShowConsumeTableViewController: UITableViewController {

    var detail:Consume!
    var consumes:[Consume]!
    
    let showDetailConsumeSegueIdentifier = "detailConsume"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.consumes=ConsumeRepository.repository.consumes
        if(self.consumes.count == 0 ){
            message()
        }else{
            invertir()
        }
       
        self.tableView.reloadData()

    }
    
    func message() ->(){
        
        let alertController = UIAlertController(title: "Attention", message:"There is not Consumption", preferredStyle: UIAlertControllerStyle.alert)
        let confirmed = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
        alertController.addAction(confirmed)
        self.present(alertController, animated: true, completion: nil)
    }
 
    
    
    func invertir()->(){
        self.consumes.reverse()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.consumes=ConsumeRepository.repository.consumes
        if(self.consumes.count == 0 ){
            message()
        }else{
            invertir()
        }
        self.tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.consumes.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "consumeCell", for: indexPath) as! ConsumeTableViewCell
        cell.labelStation.text = consumes[indexPath.row].brand?.name
        
        let formatter = DateFormatter()
        formatter.dateFormat = "E MMM dd, yyyy"
        let result = formatter.string(from: consumes[indexPath.row].date!)
        cell.labelDate.text = result// Configure the cell...
        
        return cell
    }
 
    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        detail = self.consumes[indexPath.row]
        return indexPath
    }


    // Borrar registro
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCellEditingStyle.delete{
            ConsumeRepository.repository.consumes.remove(at: (ConsumeRepository.repository.consumes.count - 1 ) - indexPath.row)  // Borra de memoria del archiving
            self.consumes.remove(at: indexPath.row)  // Borra el arreglo memoria
            self.tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
            ConsumeRepository.repository.save()  // guarda con el elemento eliminado

        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == showDetailConsumeSegueIdentifier {
            if let detailViewController = segue.destination as? DetailConsumeTableViewController {
                detailViewController.consume = detail
            }
        }
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
