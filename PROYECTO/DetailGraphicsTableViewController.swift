//
//  DetailGraphicsTableViewController.swift
//  TU GAS
//
//  Created by JUAN PABLO GUEVARA //  FABIAN CONSTANTE  on 26/12/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import UIKit

enum Example {
    case bars, groupedBars, scroll
}

class DetailGraphicsTableViewController: UITableViewController {

    var graficos: [(Example, String)]!
    var opTab:Int!
    
    
    @IBAction func backSelect(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    var tablas:[(String,Int)]!    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(ConsumeRepository.repository.consumes.count == 0 ){
            graficos = [(Example, String)] ()
            tablas = [(String,Int)] ()
            message()
        }else{
            graficos = [
                (.scroll, "Price vs Distance"),
                (.bars, "Price per Week"),
                (.groupedBars, "Price and Distance per Month")
                
            ]

            tablas = [("Price vs Distance",1),("Price for Week", 2),("Price and Distance per Month", 3)]
        }
        self.tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if(ConsumeRepository.repository.consumes.count == 0 ){
            graficos = [(Example, String)] ()
            tablas = [(String,Int)] ()
            message()
        }else{
            graficos = [
                (.scroll, "Price vs Distance"),
                (.bars, "Price per Week"),
                (.groupedBars, "Price and Distance per Month")
                
            ]

            tablas = [("Price vs Distance",1),("Price for Week", 2),("Price and Distance per Month", 3)]
        }
        self.tableView.reloadData()
    }
    
    func message() ->(){
        
        let alertController = UIAlertController(title: "Attention", message:"There is not Consumption", preferredStyle: UIAlertControllerStyle.alert)
        let confirmed = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
        alertController.addAction(confirmed)
        self.present(alertController, animated: true, completion: nil)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
         if(ConsumeRepository.repository.consumes.count > 0 ){
            switch (section) {
            case 0:
                return "Graphis"
            default: //case 2:
                return "Tables"
            }
         }else{
            return ""
        }
       
    }

    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if (section == 0){
             return graficos.count
        }else{
            return tablas.count
        }
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellGra", for: indexPath)
            cell.textLabel?.text = graficos[indexPath.row].1
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellTab", for: indexPath)
            cell.textLabel?.text = tablas[indexPath.row].0
            return cell
        }
       
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetailGra" {
            
            func showExample(_ index: Int) {
                let example = self.graficos[index]
                let controller = segue.destination as! DetailViewController
                controller.detailItem = example.0
                controller.title = example.1
                
            }
            
            if let indexPath = self.tableView.indexPathForSelectedRow {
                showExample(indexPath.row)
            } else {
                showExample(0)
            }
        }
        
        if segue.identifier == "showDetailTab" {
            
            func showExample(_ index: Int) {
                let example = self.tablas[index]
                let controller = segue.destination as! ReportTableViewController
                controller.opTab = self.opTab
                controller.title = example.0
                
            }
            
            if let indexPath = self.tableView.indexPathForSelectedRow {
                showExample(indexPath.row)
            } else {
                showExample(0)
            }
        }
        
       /* if segue.identifier == "showDetailTab" {
                let controller = segue.destination as! ReportTableViewController
                controller.opTab = self.opTab

        }*/
        
    }

    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        if (indexPath.section == 1){
            self.opTab=indexPath.row
        }
        return indexPath
    }

    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
