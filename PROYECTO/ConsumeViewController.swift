//
//  ConsumeViewController.swift
//  TU GAS
//
//  Created by JUAN PABLO GUEVARA  // FABIAN CONSTANTE  on 2/12/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import UIKit

class ConsumeViewController: UIViewController {

    var fuelStation: FuelStation!
    var brand: Brand!
    var district: District!
    var municipality: Municipality!
    var fuelType: FuelType!
    var precio:Double!
    let date = Date()
    
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var imagen: UIImageView!
    @IBOutlet weak var mileageText: UITextField!
    @IBOutlet weak var totalText: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
       
        imagen.image=brand.image
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        let result = formatter.string(from: date)
        labelDate.text = result
    }

    @IBAction func saveConsumeButton(_ sender: Any) {
        
        
        if(mileageText.text == "" || totalText.text == ""){  // Campos con valor vacio
            mileageText.text = ""
            totalText.text = ""
            print("Nilllll")
        }else{   // Campos con diferentes de vacio
            
            if(Double(mileageText.text!) == nil  || Double(totalText.text!) == nil){  // Campos direntes de Double
                mileageText.text = ""
                totalText.text = ""
                print("no es double")
            }else{          // Campos con tipo de Dato Double
                let newConsume = Consume(fuelStation: fuelStation, precioFuel: precio, date: date, mileage: Double(mileageText.text!)!, totalPay: Double(totalText.text!)!, brand: brand,fuelType:fuelType)
                ConsumeRepository.repository.consumes.append(newConsume)
                ConsumeRepository.repository.save()
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    @IBAction func cancelButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
