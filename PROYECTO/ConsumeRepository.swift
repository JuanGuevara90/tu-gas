//
//  ConsumeRepository.swift
//  TU GAS
//
//  Created by JUAN PABLO GUEVARA // FABIAN CONSTANTE on 11/12/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import Foundation

class ConsumeRepository:RepositoryProtocol {
    
    //singleton repository
    static let repository = ConsumeRepository() //shared Instance
    fileprivate init () {}
    
    var consumes = [Consume]()
    
    var documentsPath = URL(fileURLWithPath: "")
    var filePath = URL(fileURLWithPath: "")
    
    func save() {
        documentsPath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) [0])
        print(documentsPath)
        filePath = documentsPath.appendingPathComponent("consumes.data", isDirectory: false)
        print(filePath)
        
        let path = filePath.path
        
        if NSKeyedArchiver.archiveRootObject(consumes, toFile: path) {
            print("Successfully saved userconf")
        } else {
            print("Failure saving people")
        }
    }
    
    func load() {
        documentsPath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) [0])
        print(documentsPath)
        filePath = documentsPath.appendingPathComponent("consumes.data", isDirectory: false)
        print(filePath)
        
        let path = filePath.path
        
        if let newData = NSKeyedUnarchiver.unarchiveObject(withFile: path) as? [Consume]{
            consumes = newData
        }
        
    }
}
