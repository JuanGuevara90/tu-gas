//
//  PROYECTOTests.swift
//  PROYECTOTests
//
//  Created by formando on 17/11/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import XCTest
import CoreLocation
@testable import PROYECTO
import Darwin

class PROYECTOTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
    
        // *** Create date ***
        let date = Date()
   
        
        var c = NSDateComponents()
        c.year = 2016
        c.month = 12
        c.day = 31
        
        // Get NSDate given the above date components
        let date2 = NSCalendar(identifier: NSCalendar.Identifier.gregorian)?.date(from: c as DateComponents)
        
        print(date2!)
        
        
        let calendar = Calendar.current
        let now = calendar.component(.day, from: date2!)
        var before:Int = 0
        var after:Int = 0
        // *** Get components using current Local & Timezone ***
        print(now)
        /*switch now {
        case 1: // Mes actual Enero
        
            before = 12 // anterior Diciembre
            after = now+1  // proyeccion Febrero
        case 12:  //Mes actual Diciembre
            before = now-1 // anterior  Noviembre
            after = 1 //  Proyeccion Enero

        default:
            before = now-1  // anterior para los otros casos
            after = now+1  // proyeccion
        }
 
        print(before)
        print(now)
        print(after)
        */
    }
  
    
    func testPerformanceExample() {
    
        // location 1
        let latitude1:Double = 39.7492
        let longitude1:Double = -8.8077
        
        // location 2 // Lisboa
        print("Lisboa")
        var latitude2:Double = 38.71667
        var longitude2:Double = -9.13333
        
        var distance = calculateDistance(lat1: latitude1, lon1: longitude1, lat2: latitude2, lon2: longitude2)
        
        print("Distancia Formula")
        print(distance)
        
        var location2 = CLLocation(latitude: latitude2, longitude: longitude2)
        
        var startLocation = CLLocation(latitude: latitude1, longitude: longitude1)
        var distance2 = startLocation.distance(from: location2)
        
        print("Distancia CLLocation")
        print(distance2/1000)
        
        
        //++----------------
        
        latitude2 = 48.85341
        longitude2 = 2.3488
        print("Paris")
        distance = calculateDistance(lat1: latitude1, lon1: longitude1, lat2: latitude2, lon2: longitude2)
        
        print("Distancia Formula")
        print(distance)
        
        location2 = CLLocation(latitude: latitude2, longitude: longitude2)
        
        startLocation = CLLocation(latitude: latitude1, longitude: longitude1)
        distance2 = startLocation.distance(from: location2)
        
        print("Distancia CLLocation")
        print(distance2/1000)
        
        
        
        //++----------------
        
        latitude2 = 55.75222
        longitude2 = 37.61556
        print("Moscu")
        distance = calculateDistance(lat1: latitude1, lon1: longitude1, lat2: latitude2, lon2: longitude2)
        
        print("Distancia Formula")
        print(distance)
        
        location2 = CLLocation(latitude: latitude2, longitude: longitude2)
        
        startLocation = CLLocation(latitude: latitude1, longitude: longitude1)
        distance2 = startLocation.distance(from: location2)
        
        print("Distancia CLLocation")
        print(distance2/1000)
        
        //++----------------
        
        latitude2 = -33.86785
        longitude2 = 151.20732
        print("Sidney")
        distance = calculateDistance(lat1: latitude1, lon1: longitude1, lat2: latitude2, lon2: longitude2)
        
        print("Distancia Formula")
        print(distance)
        
        location2 = CLLocation(latitude: latitude2, longitude: longitude2)
        
        startLocation = CLLocation(latitude: latitude1, longitude: longitude1)
        distance2 = startLocation.distance(from: location2)
        
        print("Distancia CLLocation")
        print(distance2/1000)
        
        
        //++----------------
        
        latitude2 = -0.22985
        longitude2 = -78.52495
        print("Quito")
        distance = calculateDistance(lat1: latitude1, lon1: longitude1, lat2: latitude2, lon2: longitude2)
        
        print("Distancia Formula")
        print(distance)
        
        location2 = CLLocation(latitude: latitude2, longitude: longitude2)
        
        startLocation = CLLocation(latitude: latitude1, longitude: longitude1)
        distance2 = startLocation.distance(from: location2)
        
        print("Distancia CLLocation")
        print(distance2/1000)
        
        
        
        
    }
    
    func toRoad (valor: Double)->Double{
        return valor * M_PI / 180
    }
    
    func calculateDistance(lat1: Double,lon1:Double,lat2:Double,lon2:Double) ->Double{
        let radio:Double = 6371
        
        let diflat = toRoad(valor: lat2 - lat1)
        let diflon = toRoad(valor: lon2 - lon1)
        let a = sin(diflat/2) * sin(diflat/2) +  sin(diflon/2) * sin(diflon/2) * cos(toRoad(valor: lat1)) * cos(toRoad(valor: lat2))
        let c = 2 * asin(sqrt(a))
        let d =  radio * c
        return d
        
    }
    
    
    func testCountryFetch(){
   
    }
    
    
    
    
}
